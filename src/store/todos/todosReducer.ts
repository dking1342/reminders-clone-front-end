import { Todo } from '../../types/todos';
import { TODOS_CONSTANTS } from './todosConstants';

export const todosReducer = (
  state: Todo[],
  action: { type: string; payload: any }
) => {
  const { type, payload } = action;

  switch (type) {
    case TODOS_CONSTANTS.TODO_POPULATE_ITEMS:
      state = payload as Todo[];
      return state;

    case TODOS_CONSTANTS.TODO_ADD_ITEM:
      let todo = payload as Todo;
      state = [...state, todo];
      return state;

    case TODOS_CONSTANTS.TODO_UPDATE_ITEM:
      let updatedTodo = payload as Todo;
      state = state.map((t) => (t._id === updatedTodo._id ? updatedTodo : t));
      return state;

    case TODOS_CONSTANTS.TODO_UPDATE_ITEM_FLAG:
      let flagId: string | number = payload;
      state = state.map((todo) => {
        if (todo._id === flagId) {
          todo = {
            ...todo,
            isFlagged: !todo.isFlagged,
          };
        }
        return todo;
      });
      return state;

    case TODOS_CONSTANTS.TODO_UPDATE_ITEM_DONE:
      let doneId: string | number = payload;
      state = state.map((todo) => {
        if (todo._id === doneId) {
          todo = {
            ...todo,
            isDone: !todo.isDone,
            isFlagged: false,
          };
        }
        return todo;
      });
      return state;

    case TODOS_CONSTANTS.TODO_UPDATE_ITEMS_LIST:
      const newListName = payload as { id: string | number; name: string };

      state = state.map((todo) => {
        if (newListName.id === todo.list._id) {
          todo = {
            ...todo,
            list: { ...todo.list, name: newListName.name },
          };
        }
        return todo;
      });
      return state;

    case TODOS_CONSTANTS.TODO_DELETE_ITEM:
      const deleteId: string | number = payload;
      state = state.filter((todo) => todo._id !== deleteId);
      return state;

    case TODOS_CONSTANTS.TODO_DELETE_ITEMS_DONE:
      const listName: string = payload;

      if (listName === 'All') {
        state = state.filter((todo) => !todo.isDone);
      } else {
        state = state.filter(
          (todo) =>
            todo.list.name !== listName ||
            (todo.list.name === listName && todo.isDone === false)
        );
      }
      return state;

    case TODOS_CONSTANTS.TODO_DELETE_ITEMS_LIST:
      const deleteListId: string | number = payload;
      state = state.filter((todo) => todo.list._id !== deleteListId);
      return state;

    default:
      return state;
  }
};
