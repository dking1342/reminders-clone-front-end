export const FILTERS_CONSTANTS = {
  FILTER_POPULATE_ALL: 'FILTER_POPULATE_ALL',
  FILTER_SET_FILTERS_ALL: 'FILTER_SET_FILTERS_ALL',
  FILTER_SET_FILTERS_SEARCH: 'FILTER_SET_FILTERS_SEARCH',
  FILTER_SET_FILTERS_LIST: 'FILTER_SET_FILTERS_LIST',
  FILTER_SET_FILTERS_SHOWING: 'FILTER_SET_FILTERS_SHOWING',
  FILTER_SET_FILTERS_TAGS: 'FILTER_SET_FILTERS_TAGS',
  FILTER_TODOS: 'FILTER_TODOS',
};
