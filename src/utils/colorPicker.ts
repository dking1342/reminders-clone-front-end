type ColorPickerType = (color: string) => string;

export const colorPicker: ColorPickerType = (color) => {
  let hexValue = '';

  switch (color) {
    case '200':
      hexValue = '#F4CA46';
      break;
    case '300':
      hexValue = '#E9883E';
      break;
    case '400':
      hexValue = '#E25641';
      break;
    case '500':
      hexValue = '#D73B4A';
      break;
    case '600':
      hexValue = '#B33D35';
      break;
    case '700':
      hexValue = '#B22D4D';
      break;
    case '800':
      hexValue = '#902056';
      break;
    default:
      hexValue = '#F4CA46';
      break;
  }

  return hexValue;
};
