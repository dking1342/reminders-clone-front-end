import '../styles/sidebarFooter.css';
import { AddCircle } from '../../UI/components/AddCircle';
import { DialogButton } from '../../UI/components/DialogButton';

type Props = {};

const SidebarFooter = ({}: Props) => {
  return (
    <footer>
      <DialogButton selector="#listMenuDialog" action="POST">
        <AddCircle />
        <span className="caption-2">Add List</span>
      </DialogButton>
    </footer>
  );
};

export default SidebarFooter;
