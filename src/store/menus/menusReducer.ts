import { MenuList, MenuOptions, MenuState, MenuType } from '../../types/menus';
import { Tag } from '../../types/tags';
import { Todo } from '../../types/todos';
import { menusInitialState } from '../MenusContext';
import { MENU_CONSTANTS } from './menusConstants';

export const menusReducer = (
  state: MenuList,
  action: { type: string; payload: any }
) => {
  const { type, payload } = action;

  // menu tag methods
  const handleTagMatchingFunc = (tags: Tag[], input: string) => {
    return tags.find((tag) => tag.name === `#${input}`);
  };
  const handleAddSearchTag = (input: string) => {
    if (input) {
      handleCheckExistingTag(
        state.filteredTags,
        state.selectedTags,
        input,
        handleTagMatchingFunc
      );
      handleCheckExistingTag(
        state.selectedTags,
        state.selectedTags,
        input,
        handleTagMatchingFunc
      );
      state = { ...state, filteredTags: [] };
    }
  };
  const handleCheckExistingTag = (
    tags: Tag[],
    selectedTags: Tag[],
    input: string,
    matchFn: (tags: Tag[], input: string) => Tag | undefined
  ) => {
    const matchingTag = matchFn(tags, input);
    return matchingTag
      ? handleAddSearchToTags(matchingTag, selectedTags)
      : handleCreateNewTag(input, selectedTags);
  };
  const handleAddSearchToTags = (tag: Tag, selectedTags: Tag[]) => {
    const noDuplicateSelectedTags: Tag[] = [...new Set([...selectedTags, tag])];
    state = { ...state, selectedTags: noDuplicateSelectedTags };
  };
  const handleCreateNewTag = (input: string, selectedTags: Tag[]) => {
    const newTag: Tag = {
      _id: Math.ceil(Math.random() * 1000000),
      isSelected: false,
      name: `#${input.toLowerCase()}`,
    };
    state = { ...state, selectedTags: [...selectedTags, newTag] };
  };

  switch (type) {
    case MENU_CONSTANTS.MENU_LIST_SET_ALL:
      const listAll = payload as MenuOptions;
      state = { ...state, list: listAll };
      return state;

    case MENU_CONSTANTS.MENU_LIST_SET_STATE:
      const listState = payload as MenuState;
      state = { ...state, list: { ...state.list, state: listState } };
      return state;

    case MENU_CONSTANTS.MENU_LIST_SET_IS_OPEN:
      const listIsOpen = payload as boolean;
      state = { ...state, list: { ...state.list, isOpen: listIsOpen } };
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_ALL:
      const todoAll = payload as MenuOptions;
      state = { ...state, todo: todoAll };
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_STATE:
      const todoState = payload as MenuState;
      state = { ...state, todo: { ...state.todo, state: todoState } };
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_IS_OPEN:
      const todoIsOpen = payload as boolean;
      state = { ...state, todo: { ...state.todo, isOpen: todoIsOpen } };
      return state;

    case MENU_CONSTANTS.MENU_TYPE_SET:
      const menuType = payload as MenuType;
      state = { ...state, type: menuType };
      return state;

    case MENU_CONSTANTS.MENU_STATE_RESET:
      state = menusInitialState;
      return state;

    // tags
    case MENU_CONSTANTS.MENU_TODO_SET_TAG_INPUT:
      let { e, tags } = payload as { e: string; tags: Tag[] };
      state = { ...state, tagInput: e };
      if (e.length > 0) {
        const filteredTags = tags.filter((tag) => tag.name.includes(e));
        const selectedTags = state.selectedTags;
        const resultTags = [...filteredTags, ...selectedTags].reduce(
          (acc: Tag[], val: Tag) => {
            let isInAcc = acc.find((t) => t.name === val.name);
            let isSelectedTag = selectedTags.find((t) => t.name === val.name);

            // not in acc and selected
            if (!isInAcc && !isSelectedTag) {
              acc.push(val);
            }
            return acc;
          },
          []
        );
        state = { ...state, filteredTags: resultTags };
      } else {
        state = { ...state, filteredTags: [] };
      }
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_SELECTED_TAGS:
      const { id: setId, todos: setTodos } = payload as {
        id: string | number;
        todos: Todo[];
      };
      const getTodo = setTodos.find((l) => l._id === setId) as Todo;
      getTodo
        ? (state = { ...state, selectedTags: [...getTodo.tags] })
        : (state = { ...state, selectedTags: [] });
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_SELECTED_TAGS_ADD:
      const { input, tags: tagsAll } = payload as {
        input: string;
        tags: Tag[];
      };

      const checker = (tag: Tag, text: string) =>
        tag.name.toLowerCase().substring(1) === text.toLowerCase();
      // check if existing tag exists
      const isExistingTag = tagsAll.find((t) => checker(t, input));
      const isSelectedTag = state.selectedTags.find((t) => checker(t, input));

      if (!isSelectedTag) {
        if (isExistingTag) {
          state = {
            ...state,
            selectedTags: [...state.selectedTags, isExistingTag],
            filteredTags: [],
            tagInput: '',
          };
        } else {
          const name = `#${input}`.toLowerCase();
          const newTag: Tag = {
            _id: Math.ceil(Math.random() * 100000),
            name,
            isSelected: false,
          };
          state = {
            ...state,
            selectedTags: [...state.selectedTags, newTag],
            filteredTags: [],
            tagInput: '',
          };
        }
      } else {
        state = { ...state, tagInput: '' };
      }
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_SELECTED_TAGS_UPDATE:
      let savedTag = payload as Tag;
      let updatedSelectedTags = state.selectedTags.map((tag) => {
        if (tag.name === savedTag.name) {
          tag = { ...tag, _id: savedTag._id };
        }
        return tag;
      });
      state = { ...state, selectedTags: updatedSelectedTags };
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_SELECTED_TAGS_CLICK:
      let { id, allTags } = payload as {
        id: number;
        allTags: Tag[];
      };
      const selectedTag: Tag = state.filteredTags.find(
        (t) => t._id === id
      ) as Tag;
      const findTag: Tag | undefined = state.selectedTags.find(
        (tag) => tag._id === id
      );
      if (!findTag) {
        state = {
          ...state,
          filteredTags: [],
          selectedTags: [...state.selectedTags, selectedTag],
          tagInput: '',
        };
      }
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_SELECTED_TAGS_DELETE:
      let deleteId = payload as number;
      let selectedDeleteTags = state.selectedTags.filter(
        (tag) => tag._id !== deleteId
      );
      state = { ...state, selectedTags: selectedDeleteTags };
      return state;

    case MENU_CONSTANTS.MENU_STATE_SUBMITTED:
      let submitInput = payload as boolean;
      state = { ...state, submitted: submitInput };
      return state;

    case MENU_CONSTANTS.MENU_STATE_ERROR:
      let error = payload as string;
      state = { ...state, errors: error };
      return state;

    default:
      return state;
  }
};
