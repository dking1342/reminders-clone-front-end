import '@testing-library/jest-dom/extend-expect';
import { screen, render } from '@testing-library/react';
import { ReactElement, JSXElementConstructor } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { MobileContext } from '../../../store/MobileContext';
import Sidebar from '../components/Sidebar';

const customRender = (
  ui: ReactElement<any, string | JSXElementConstructor<any>>,
  { providerProps, ...renderOptions }: any
) => {
  return render(
    <QueryClientProvider client={new QueryClient()}>
      <MobileContext.Provider value={providerProps}>
        {ui}
      </MobileContext.Provider>
    </QueryClientProvider>,
    renderOptions
  );
};

describe('Sidebar Component Testing', () => {
  test('When isMobileList is false there is a class sidebar-container-closed', () => {
    const providerProps = {
      ...MobileContext,
      isMobileList: false,
    };
    const className = 'sidebar-container-closed';
    customRender(<Sidebar />, { providerProps });
    const elementClassName = screen
      .getByTestId('sidebar')
      .getAttribute('class')
      ?.split(' ')
      .find((el) => el === className);
    expect(elementClassName).toBe(className);
  });
  test('When isMobileList is true there is not a class sidebar-container-closed', () => {
    const providerProps = {
      ...MobileContext,
      isMobileList: true,
    };
    const className = 'sidebar-container-closed';
    customRender(<Sidebar />, { providerProps });
    const elementClassName = screen
      .getByTestId('sidebar')
      .getAttribute('class')
      ?.split(' ')
      .find((el) => el === className);
    expect(elementClassName).not.toBe(className);
  });
});
