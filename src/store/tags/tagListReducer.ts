import { Tag } from '../../types/tags';
import { TAGS_CONSTANTS } from './tagsConstants';

export const tagListReducer = (
  state: Tag[],
  action: { type: string; payload: any }
) => {
  const { type, payload } = action;

  switch (type) {
    case TAGS_CONSTANTS.GET_ALL_TAGS:
      const tagList: Tag[] = payload as Tag[];
      state = tagList;
      return state;

    case TAGS_CONSTANTS.ADD_ALL_TAGS:
      const tag: Tag = payload as Tag;
      state = [...state, tag];
      return state;

    default:
      return state;
  }
};
