import { useContext, useEffect } from 'react';
import { FiltersContext } from '../../../store/FiltersContext';
import ErrorMessage from '../../errors/components/ErrorMessage';
import '../styles/todosContainer.css';
import { TodoItem } from './TodoItem';

type TodosContainerProps = {};

export const TodosContainer = ({}: TodosContainerProps) => {
  const { filteredTodos } = useContext(FiltersContext);

  if (filteredTodos.length) {
    return (
      <div className="todos-container">
        {filteredTodos.map((todo) => (
          <TodoItem key={todo._id} todo={todo} />
        ))}
      </div>
    );
  } else {
    return (
      <ErrorMessage height="64px" alignment="center">
        <p className="headline-3">No Todos</p>
      </ErrorMessage>
    );
  }
};
