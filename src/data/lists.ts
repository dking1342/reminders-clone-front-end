import { List } from '../types/lists';

export const listData: List[] = [
  {
    _id: 1,
    name: 'All',
    color: '200',
    createdAt: Date.now(),
    updatedAt: Date.now(),
  },
  {
    _id: 2,
    name: 'Flagged',
    color: '300',
    createdAt: Date.now(),
    updatedAt: Date.now(),
  },
  {
    _id: 3,
    name: 'Chores',
    color: '200',
    createdAt: Date.now(),
    updatedAt: Date.now(),
  },
  {
    _id: 4,
    name: 'Homework',
    color: '500',
    createdAt: Date.now() + 10000,
    updatedAt: Date.now() + 10000,
  },
];

export const filterData: List[] = [
  {
    _id: 1,
    name: 'All',
    color: '200',
    createdAt: Date.now(),
    updatedAt: Date.now(),
  },
  {
    _id: 2,
    name: 'Flagged',
    color: '300',
    createdAt: Date.now(),
    updatedAt: Date.now(),
  },
];
