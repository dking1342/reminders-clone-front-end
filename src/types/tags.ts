export type Tag = {
  _id?: string | number;
  name: string;
  isSelected: boolean;
};
