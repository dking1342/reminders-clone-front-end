import { useContext } from 'react';
import { MenusContext } from '../../../store/MenusContext';
import '../styles/menuCancelButton.css';

type MenuCancelButtonProps = {
  form: HTMLFormElement;
  formType?: 'list' | 'todo';
};

export const MenuCancelButton = ({ form, formType }: MenuCancelButtonProps) => {
  const {
    handleMenuListSetIsOpen,
    handleMenuTodoSetIsOpen,
    handleResetListMenuFormValues,
    handleResetTodoMenuFormValues,
    handleMenuSetTags,
    handleMenuError,
  } = useContext(MenusContext);

  const handleClear = () => {
    if (formType === 'list') {
      handleMenuListSetIsOpen(false);
      handleResetListMenuFormValues();
    } else {
      handleMenuTodoSetIsOpen(false);
      handleResetTodoMenuFormValues();
      handleMenuSetTags(0, []);
    }
    handleMenuError('');
    form.reset();
  };
  return (
    <button
      className="menu-button caption-2"
      value="cancel"
      onClick={handleClear}
    >
      Cancel
    </button>
  );
};
