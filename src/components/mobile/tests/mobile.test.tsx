import '@testing-library/jest-dom/extend-expect';
import { screen, render } from '@testing-library/react';
import { ReactElement, JSXElementConstructor } from 'react';
import { MobileContext } from '../../../store/MobileContext';
import Mobile from '../components/Mobile';

const setup = (
  ui: ReactElement<any, string | JSXElementConstructor<any>>,
  { providerProps, ...renderOptions }: any
) => {
  return render(
    <MobileContext.Provider value={providerProps}>{ui}</MobileContext.Provider>,
    renderOptions
  );
};

describe('Mobile Component Tests', () => {
  test('Button in nav has text Todos when isMobileList is true', () => {
    const isMobileList = true;
    const providerProps = {
      ...MobileContext,
      isMobileList,
    };
    const text = /^todos/i;
    setup(<Mobile />, { providerProps });
    const btn = screen.getByTestId('mobile-btn');
    expect(btn).toBeInTheDocument();
    expect(btn.textContent).toMatch(text);
  });
  test('Button in nav has text Lists when isMobileList is false', () => {
    const isMobileList = false;
    const providerProps = {
      ...MobileContext,
      isMobileList,
    };
    const text = /lists$/i;
    setup(<Mobile />, { providerProps });
    const btn = screen.getByTestId('mobile-btn');
    expect(btn).toBeInTheDocument();
    expect(btn.textContent).toMatch(text);
  });
});
