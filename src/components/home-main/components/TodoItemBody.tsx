import '../styles/todoItemBody.css';
import { Tag } from '../../../types/tags';
import { DialogButton } from '../../UI/components/DialogButton';
import { EditIcons } from '../../UI/components/EditIcons';
import { ListItemFlag } from './ListItemFlag';
import { TodoItemTags } from './TodoItemTags';

type TodoItemBodyProps = {
  name: string;
  tags: Tag[];
  id: string | number | undefined;
  isFlaggedIcon: boolean;
  isDone: boolean;
};

export const TodoItemBody = ({
  name,
  tags,
  id,
  isFlaggedIcon,
  isDone,
}: TodoItemBodyProps) => {
  return (
    <div className="todos-item-body">
      <div className="todos-item-body-name">
        <span
          data-testid="todoItemBody-span"
          className={`body-1 ${isDone && 'isDone'}`}
        >
          {name}
        </span>
        {!isDone && (
          <DialogButton action="PUT" selector="#todoMenuDialog" id={id}>
            <EditIcons size="small" />
          </DialogButton>
        )}
      </div>
      <TodoItemTags tags={tags} isDone={isDone} />
      {!isDone && <ListItemFlag id={id} isFlaggedIcon={isFlaggedIcon} />}
    </div>
  );
};
