import { useContext, useEffect, useState } from 'react';
import { useMutation } from 'react-query';
import { todoApiDeleteUrl } from '../../../api/todos/constants/urls';
import { fetchTodos } from '../../../api/todos/fetchRequest';
import { FiltersContext } from '../../../store/FiltersContext';
import { ListsContext } from '../../../store/ListsContext';
import { TodosContext } from '../../../store/TodosContext';
import { Todo } from '../../../types/todos';
import '../styles/mainHeaderSubTitle.css';

type Props = {};

const MainHeaderSubTitle = ({}: Props) => {
  const { filter, filteredTodos, handleFiltersSetShowing } =
    useContext(FiltersContext);
  const { todos, handleDeleteTodosDone, handleDeleteTodo } =
    useContext(TodosContext);
  const { listName } = useContext(ListsContext);
  const [quantity, setQuantity] = useState(0);

  const handleTodoQuantity = ({ name }: { name: string }) => {
    switch (name) {
      case 'All':
        const allTodos = filteredTodos.filter((todo) => todo.isDone);
        setQuantity(allTodos.length);
        break;
      case 'Flagged':
        const flaggedTodos = filteredTodos
          .filter((todo) => todo.isFlagged)
          .filter((todo) => todo.isDone);
        setQuantity(flaggedTodos.length);
        break;
      default:
        const completedTodos = filteredTodos
          .filter((todo) => todo.list.name === listName.name)
          .filter((todo) => todo.isDone);
        setQuantity(completedTodos.length);
        break;
    }
  };

  const deleteMutation = useMutation(
    (todoId: string | number) => {
      const url = todoApiDeleteUrl(todoId);
      return fetchTodos(url, 'DELETE');
    },
    {
      onSuccess: (data, variables) => {
        if (data.status === 204) {
          handleDeleteTodo(variables);
        }
      },
    }
  );

  const handleBulkDelete = () => {
    // filter to get the todos that are done
    let todosToDelete: Todo[] = [];
    if (listName.name === 'All') {
      todosToDelete = todos.filter((todo) => todo.isDone);
    } else {
      todosToDelete = todos.filter(
        (todo) => todo.list.name === listName.name && todo.isDone
      );
    }

    // delete the todos that are done
    if (todosToDelete.length) {
      todosToDelete.forEach((todo) => {
        deleteMutation.mutate(todo._id!);
      });
    }
  };

  useEffect(() => {
    handleTodoQuantity(listName);
  }, [filteredTodos]);

  return (
    <div className="main-header-subtitle-container">
      <div className="main-header-subtitle-sub-container">
        <span className="subtitle-1" data-testid="mainHeaderSubTitle-quantity">
          {quantity} Completed
        </span>
        <button
          role={'button'}
          id="header-clear-button"
          onClick={() => {
            handleBulkDelete();
            handleDeleteTodosDone(listName.name);
          }}
        >
          Clear
        </button>
      </div>
      <button
        id="header-show-hide-toggle-button"
        role={'button'}
        data-testid={'mainHeaderSubTitle-hide-show'}
        onClick={() => {
          handleFiltersSetShowing();
        }}
      >
        {filter.isShowing ? 'Hide' : 'Show'}
      </button>
    </div>
  );
};

export default MainHeaderSubTitle;
