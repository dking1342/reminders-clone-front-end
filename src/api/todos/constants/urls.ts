type IdType = string | number | undefined;

const baseUrl = 'http://157.230.217.83:80/api/todos';

export const todoApiGetAllUrl = `${baseUrl}/get/all`;
export const todoApiPostUrl = `${baseUrl}/create`;
export const todoApiGetOneUrl = (id: IdType) => {
  return `${baseUrl}/get/one/${id}`;
};
export const todoApiPutUrl = (id: IdType) => {
  return `${baseUrl}/update/${id}`;
};
export const todoApiDeleteUrl = (id: IdType) => {
  return `${baseUrl}/delete/${id}`;
};
