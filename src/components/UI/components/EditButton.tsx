import '../styles/editButton.css';
import { useState, useEffect } from 'react';

type EditButtonProps = {
  color?: 'gray' | 'yellow' | 'red';
  text: string;
  id: string | number | undefined;
  fn: (id: string | number) => void;
};

export const EditButton = ({ color, text, fn, id }: EditButtonProps) => {
  const [fill, setFill] = useState('#636363');
  const [textColor, setTextColor] = useState('#ffffff');

  useEffect(() => {
    switch (color) {
      case 'gray':
        setFill('#636363');
        setTextColor('#ffffff');
        break;
      case 'red':
        setFill('#FF5050');
        setTextColor('#ffffff');
        break;
      case 'yellow':
        setFill('#FFD338');
        setTextColor('#000000');
        break;
      default:
        setFill('#636363');
        break;
    }
  }, []);

  return (
    <button
      className="caption-1 edit-button"
      style={{ backgroundColor: fill, color: textColor }}
      onClick={() => fn(id!)}
    >
      {text}
    </button>
  );
};
