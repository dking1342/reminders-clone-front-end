import { createContext, useEffect, useReducer } from 'react';
import { useQuery } from 'react-query';
import { todoApiGetAllUrl } from '../api/todos/constants/urls';
import { fetchTodos } from '../api/todos/fetchRequest';
import { todosData } from '../data/todos';
import { Todo } from '../types/todos';
import { TODOS_CONSTANTS } from './todos/todosConstants';
import { todosIdReducer } from './todos/todosIdReducer';
import { todosReducer } from './todos/todosReducer';

export const todosInitialState: Todo[] = [];
export const todosIdInitialState: string | number = 0;

export const TodosContext = createContext({
  todos: todosInitialState,
  // todosId: '',
  handlePopulateTodos: (todos: Todo[]) => {},
  handleAddTodo: (todo: Todo) => {},
  handleUpdateTodo: (todo: Todo) => {},
  handleUpdateTodoFlag: (id: string | number | undefined) => {},
  handleUpdateTodoDone: (id: string | number | undefined) => {},
  handleUpdateTodosList: ({
    id,
    name,
  }: {
    id: string | number;
    name: string;
  }) => {},
  handleDeleteTodo: (id: string | number) => {},
  handleDeleteTodosDone: (list: string) => {},
  handleDeleteTodosList: (id: string | number | undefined) => {},
  // handleSetTodoId: (id: string | number | undefined) => {},
});

type TodosProviderProps = {
  children: any;
};

export const TodosProvider = ({ children }: TodosProviderProps) => {
  const [state, dispatch] = useReducer(todosReducer, todosInitialState);
  // const [idState, idDispatch] = useReducer(todosIdReducer, todosIdInitialState);

  // actions
  const handlePopulateTodos = (todos: Todo[]) => {
    dispatch({
      type: TODOS_CONSTANTS.TODO_POPULATE_ITEMS,
      payload: todos,
    });
  };
  const handleAddTodo = (todo: Todo) => {
    dispatch({
      type: TODOS_CONSTANTS.TODO_ADD_ITEM,
      payload: todo,
    });
  };
  const handleUpdateTodo = (todo: Todo) => {
    dispatch({
      type: TODOS_CONSTANTS.TODO_UPDATE_ITEM,
      payload: todo,
    });
  };
  const handleUpdateTodoFlag = (id: string | number | undefined) => {
    dispatch({
      type: TODOS_CONSTANTS.TODO_UPDATE_ITEM_FLAG,
      payload: id,
    });
  };
  const handleUpdateTodoDone = (id: string | number | undefined) => {
    dispatch({
      type: TODOS_CONSTANTS.TODO_UPDATE_ITEM_DONE,
      payload: id,
    });
  };
  const handleUpdateTodosList = (listName: {
    id: string | number;
    name: string;
  }) => {
    dispatch({
      type: TODOS_CONSTANTS.TODO_UPDATE_ITEMS_LIST,
      payload: listName,
    });
  };
  const handleDeleteTodo = (id: string | number) => {
    dispatch({
      type: TODOS_CONSTANTS.TODO_DELETE_ITEM,
      payload: id,
    });
  };
  const handleDeleteTodosDone = (list: string) => {
    dispatch({
      type: TODOS_CONSTANTS.TODO_DELETE_ITEMS_DONE,
      payload: list,
    });
  };
  const handleDeleteTodosList = (id: string | number | undefined) => {
    dispatch({
      type: TODOS_CONSTANTS.TODO_DELETE_ITEMS_LIST,
      payload: id,
    });
  };

  // const handleSetTodoId = (id: string) => {
  //   idDispatch({
  //     type: TODOS_CONSTANTS.TODO_SET_ID,
  //     payload: id,
  //   });
  // };

  // api call
  const { isSuccess, data } = useQuery(
    'todos',
    async () => await fetchTodos(todoApiGetAllUrl)
  );
  useEffect(() => {
    if (isSuccess) {
      if (data.data) {
        const todos = data.data as Todo[];
        handlePopulateTodos(todos);
      }
    }
  }, [isSuccess, data]);

  const value = {
    todos: state,
    // todosId: idState,
    handlePopulateTodos,
    handleAddTodo,
    handleUpdateTodo,
    handleUpdateTodoFlag,
    handleUpdateTodoDone,
    handleUpdateTodosList,
    handleDeleteTodo,
    handleDeleteTodosDone,
    handleDeleteTodosList,
    // handleSetTodoId,
  };

  return (
    <TodosContext.Provider value={value}>{children}</TodosContext.Provider>
  );
};
