import { useContext } from 'react';
import { MobileContext } from '../../../store/MobileContext';
import '../styles/main.css';
import MainHeader from './MainHeader';
import { TodosContainer } from './TodosContainer';

type MainProps = {};

const Main = ({}: MainProps) => {
  // context
  const { isMobileList } = useContext(MobileContext);

  return (
    <section
      role={'main'}
      data-testid="main"
      className={`main ${isMobileList ? null : 'main-open'}`}
    >
      <MainHeader />
      <TodosContainer />
    </section>
  );
};

export default Main;
