import { describe, expect, test } from '@jest/globals';
import { colorPicker } from '../colorPicker';

const colorArray = ['200', '300', '400', '500', '600', '700', '800'];
const colorMap = new Map();
colorMap.set('200', '#F4CA46');
colorMap.set('300', '#E9883E');
colorMap.set('400', '#E25641');
colorMap.set('500', '#D73B4A');
colorMap.set('600', '#B33D35');
colorMap.set('700', '#B22D4D');
colorMap.set('800', '#902056');

const colorPickerTest = (color: string) => {
  const mapHexValue = colorMap.get(color);
  const funcHexValue = colorPicker(color);
  return mapHexValue === funcHexValue;
};

describe('ColorPicker function tests', () => {
  test('', () => {
    colorArray.forEach((color) => {
      const result = colorPickerTest(color);
      expect(result).toBe(true);
    });
  });
});
