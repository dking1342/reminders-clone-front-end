import { MenuTodo } from '../../types/menus';
import { ActionType } from '../../types/reducers';
import { Todo } from '../../types/todos';
import { todoMenuFormValuesIntialState } from '../MenusContext';
import { MENU_CONSTANTS } from './menusConstants';

export const todoMenuFormValuesReducer = (
  state: MenuTodo,
  action: ActionType
) => {
  const { type, payload } = action;

  switch (type) {
    case MENU_CONSTANTS.MENU_TODO_GET_FORM_VALUES:
      let { id, todos } = payload as {
        id: string | number;
        todos: Todo[];
      };
      const getTodo = todos.find((l) => l._id === id) as Todo;
      const tagsArray: string[] | number[] = getTodo.tags.map(
        (tag) => tag._id
      ) as string[] | number[];

      state = {
        ...state,
        _id: id,
        isDone: getTodo.isDone,
        isFlagged: getTodo.isFlagged,
        list: getTodo.list._id,
        name: getTodo.name,
        tags: tagsArray,
      };
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_FORM_VALUES:
      const { input, key, inputType } = payload as {
        input: string;
        key: string;
        inputType: string;
      };

      for (const k in state) {
        if (inputType === 'checkbox') {
          let value = input === 'true' ? true : false;
          state = { ...state, [key]: !value };
        } else {
          state = { ...state, [key]: input };
        }
      }
      return state;

    case MENU_CONSTANTS.MENU_TODO_RESET_FORM_VALUES:
      state = todoMenuFormValuesIntialState;
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_TAGS_FORM_VALUES:
      const tags = payload as string[];
      state = { ...state, tags: [...tags] };
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_ID_FORM_VALUES:
      const existingId = payload as string | number | undefined;
      if (existingId) {
        state = { ...state, _id: existingId };
      }
      return state;

    case MENU_CONSTANTS.MENU_TODO_SET_LIST_FORM_VALUES:
      const list = payload as string;
      state = { ...state, list };
      return state;

    default:
      return state;
  }
};
