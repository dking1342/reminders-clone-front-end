import { render, screen } from '@testing-library/react';
import { Tag } from '../../../types/tags';
import { ListItemTag } from '../components/ListItemTag';

type ListItemTagProps = {
  tag: Tag;
  isDone: boolean;
};

let testProps: ListItemTagProps = {
  tag: { _id: 1123, name: 'list item', isSelected: false },
  isDone: false,
};

const setup = () => {
  render(<ListItemTag tag={testProps.tag} isDone={testProps.isDone} />);
};

describe('ListItemTag Component Testing', () => {
  test('Span element has isDone class when isDone prop is true', () => {
    testProps = { ...testProps, isDone: true };
    setup();
    const classes = screen
      .getByTestId('listItemTag')
      .getAttribute('class')
      ?.split(' ')
      .includes('isDone');
    expect(classes).toBeTruthy();
  });
  test('Span element does not have isDone class when isDone prop is false', () => {
    testProps = { ...testProps, isDone: false };
    setup();
    const classes = screen
      .getByTestId('listItemTag')
      .getAttribute('class')
      ?.split(' ')
      .includes('isDone');
    expect(classes).toBeFalsy();
  });
  test('Span element has text of "list item" from the tag prop', () => {
    testProps = { ...testProps, tag: { ...testProps.tag, name: 'list items' } };
    setup();
    const span = screen.getByTestId('listItemTag').textContent;
    expect(span).toBe(testProps.tag.name);
  });
});
