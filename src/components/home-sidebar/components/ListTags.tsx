import { MenuAction } from '../../../types/menus';
import { Tag } from '../../../types/tags';
import '../styles/listTags.css';
import { TagItem } from './TagItem';

type ListTagsProps = {
  tags: Tag[];
  action?: MenuAction;
};

export const ListTags = ({ tags, action }: ListTagsProps) => {
  return (
    <div className="tag-wrapper">
      <>
        {tags.map((tag) => (
          <TagItem key={tag._id} tag={tag} tags={tags} action={action} />
        ))}
      </>
    </div>
  );
};
