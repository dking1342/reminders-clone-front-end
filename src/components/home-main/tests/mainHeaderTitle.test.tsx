import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { JSXElementConstructor, ReactElement } from 'react';
import { listData } from '../../../data/lists';
import { todosData } from '../../../data/todos';
import { FiltersContext } from '../../../store/FiltersContext';
import { ListsContext } from '../../../store/ListsContext';
import { colorPicker } from '../../../utils/colorPicker';
import MainHeaderTitle from '../components/MainHeaderTitle';

const customAllRender = (
  ui: ReactElement<any, string | JSXElementConstructor<any>>,
  { listProps, filtersProps, ...renderOptions }: any
) => {
  return render(
    <FiltersContext.Provider value={filtersProps}>
      <ListsContext.Provider value={listProps}>{ui}</ListsContext.Provider>
    </FiltersContext.Provider>,
    renderOptions
  );
};

describe('MainHeaderTitle Component Testing', () => {
  test('Title header showing All', () => {
    const name = 'All';
    const listProps = {
      ...ListsContext,
      lists: listData,
      listName: { name, id: 0 },
    };
    const filtersProps = {
      ...FiltersContext,
      filteredTodos: todosData,
    };
    customAllRender(<MainHeaderTitle />, { listProps, filtersProps });
    const title = screen.getByRole('heading', { name }).textContent;
    expect(title).toBe(name);
  });
  test('Title header showing Chores', () => {
    const name = 'Chores';
    const listProps = {
      ...ListsContext,
      lists: listData,
      listName: { name, id: 0 },
    };
    const filtersProps = {
      ...FiltersContext,
      filteredTodos: todosData,
    };
    customAllRender(<MainHeaderTitle />, { listProps, filtersProps });
    const title = screen.getByRole('heading', { name }).textContent;
    expect(title).toBe(name);
  });
  test('Color for list name of All is #F4CA46', () => {
    const name = 'All';
    const color = '#F4CA46';
    const listProps = {
      ...ListsContext,
      lists: listData,
      listName: { name: 'All', id: 0 },
    };
    const filtersProps = {
      ...FiltersContext,
      filteredTodos: todosData,
    };
    customAllRender(<MainHeaderTitle />, { listProps, filtersProps });
    let uiColor = screen
      .getByRole('heading', { name })
      .getAttribute('style')
      ?.match(/[0-9]+\,\s[0-9]+\,\s[0-9]+/i)
      ?.toString()
      .split(', ')
      .map((x) => Number(x).toString(16))
      .join('')
      .toUpperCase();

    uiColor = `#${uiColor}`;
    const listColor = listProps.lists.find((list) => list.name === name)?.color;
    let expectedColor = listColor ? colorPicker(listColor) : null;
    expect(expectedColor).toBe(color);
  });
  test('Color for list name of Homework is #D73B4A', () => {
    const name = 'Homework';
    const color = '#D73B4A';
    const listProps = {
      ...ListsContext,
      lists: listData,
      listName: { name, id: 0 },
    };
    const filtersProps = {
      ...FiltersContext,
      filteredTodos: todosData,
    };
    customAllRender(<MainHeaderTitle />, { listProps, filtersProps });
    let uiColor = screen
      .getByRole('heading', { name })
      .getAttribute('style')
      ?.match(/[0-9]+\,\s[0-9]+\,\s[0-9]+/i)
      ?.toString()
      .split(', ')
      .map((x) => Number(x).toString(16))
      .join('')
      .toUpperCase();

    uiColor = `#${uiColor}`;
    const listColor = listProps.lists.find((list) => list.name === name)?.color;
    let expectedColor = listColor ? colorPicker(listColor) : null;
    expect(expectedColor).toBe(color);
  });
  test('Quantity of the header for the list name of Homework is 2', () => {
    const name = 'Homework';
    const filteredTodos = todosData.filter((t) => t.list.name === name);
    const listProps = {
      ...ListsContext,
      lists: listData,
      listName: { name, id: 0 },
      filteredTodos,
    };
    const filtersProps = {
      ...FiltersContext,
      filteredTodos,
    };
    customAllRender(<MainHeaderTitle />, { listProps, filtersProps });
    const uiQuantity = screen.getByTestId('mainHeaderTitle-header2');
    expect(Number(uiQuantity.textContent)).toBe(filteredTodos.length);
  });
  test('Color for Homework quantity element is #D73B4A', () => {
    const name = 'Homework';
    const color = '#D73B4A';
    const filteredTodos = todosData.filter((t) => t.list.name === name);
    const listColor = listData.find((list) => list.name === name)?.color;
    const expectedColor = listColor ? colorPicker(listColor) : null;
    const listProps = {
      ...ListsContext,
      lists: listData,
      listName: { name, id: 0 },
      filteredTodos,
    };
    const filtersProps = {
      ...FiltersContext,
      filteredTodos,
    };
    customAllRender(<MainHeaderTitle />, { listProps, filtersProps });
    let uiQuantity = screen
      .getByTestId('mainHeaderTitle-header2')
      .getAttribute('style')
      ?.match(/[0-9]+\,\s[0-9]+\,\s[0-9]+/i)
      ?.toString()
      .split(', ')
      .map((x) => Number(x).toString(16))
      .join('')
      .toUpperCase();

    uiQuantity = `#${uiQuantity}`;
    expect(expectedColor).toBe(color);
  });
});
