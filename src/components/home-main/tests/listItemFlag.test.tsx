import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, screen } from '@testing-library/react';
import { JSXElementConstructor, ReactElement, ReactFragment } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { todosData } from '../../../data/todos';
import { TodosContext } from '../../../store/TodosContext';
import { ListItemFlag } from '../components/ListItemFlag';

const customRender = (
  ui: ReactElement<any, string | JSXElementConstructor<any>> | ReactFragment,
  { providerProps, ...renderOptions }: any
) => {
  return render(
    <QueryClientProvider client={new QueryClient()}>
      <TodosContext.Provider value={providerProps}>{ui}</TodosContext.Provider>
    </QueryClientProvider>,
    renderOptions
  );
};

describe('ListItemFlag Component Testing', () => {
  test('Component has a button child component', () => {
    const providerProps = {
      ...TodosContext,
      todos: todosData,
    };
    customRender(<ListItemFlag id={1123} isFlaggedIcon={false} />, {
      providerProps,
    });
    expect(screen.getByRole('contentinfo')).not.toBeEmptyDOMElement();
    expect(screen.getByRole('button')).toBeInTheDocument();
  });
});
