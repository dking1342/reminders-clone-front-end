import { useContext } from 'react';
import { MobileContext } from '../../../store/MobileContext';
import '../styles/mobile.css';

type Props = {};

const Mobile = ({}: Props) => {
  const { isMobileList, handleMobileToggle } = useContext(MobileContext);
  return (
    <nav className="caption-2">
      <button onClick={handleMobileToggle} data-testid="mobile-btn">
        {isMobileList ? (
          <span>Todos &#x2192;</span>
        ) : (
          <span>&#x2190; Lists</span>
        )}
      </button>
    </nav>
  );
};

export default Mobile;
