import { useContext, useEffect } from 'react';
import { useQuery } from 'react-query';
import { listApiGetAllUrl } from '../../../api/lists/constants/urls';
import { fetchLists } from '../../../api/lists/fetchRequests';
import { ListsContext } from '../../../store/ListsContext';
import { List } from '../../../types/lists';
import ErrorMessage from '../../errors/components/ErrorMessage';
import '../styles/listsContainer.css';
import { Lists } from './Lists';

type ListContainerProps = {};

export const ListsContainer = ({}: ListContainerProps) => {
  const { handlePopulateLists } = useContext(ListsContext);

  const { isLoading, isError, isSuccess, data } = useQuery(
    'lists',
    async () => await fetchLists(listApiGetAllUrl)
  );
  useEffect(() => {
    if (isSuccess && data.data) {
      let arr: List[] = [];
      Array.isArray(data.data) ? (arr = data.data) : (arr = [data.data]);
      handlePopulateLists(arr);
    } else {
      // handlePopulateLists([]);
    }
  }, [isSuccess]);

  if (isLoading) {
    return (
      <div className="list-container">
        <h1 className="caption-1">My Lists</h1>
        <ErrorMessage height="24px">
          <p className="body-1">Loading...</p>
        </ErrorMessage>
      </div>
    );
  }

  if (isError) {
    <div className="list-container">
      <h1 className="caption-1">My Lists</h1>
      <ErrorMessage height="24px">
        <p className="body-1">No Lists</p>
      </ErrorMessage>
    </div>;
  }

  if (isSuccess) {
    return (
      <div className="list-container">
        <h1 className="caption-1">My Lists</h1>
        <Lists />
      </div>
    );
  }

  return (
    <div className="list-container">
      <h1 className="caption-1">My Lists</h1>
      <ErrorMessage height="24px">
        <p className="body-1">No Lists</p>
      </ErrorMessage>
    </div>
  );
};
