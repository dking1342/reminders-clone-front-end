import { useContext } from 'react';
import { filterData } from '../../../data/lists';
import { FiltersContext } from '../../../store/FiltersContext';
import { ListsContext } from '../../../store/ListsContext';
import { MobileContext } from '../../../store/MobileContext';
import '../styles/cards.css';
import { FlagIcon } from './FlagIcon';
import { ListIcon } from './ListIcon';

type CardsProps = {
  name: string;
  quantity: number;
  icon: 'list' | 'flag';
};

export const ListCards = ({ name, quantity, icon }: CardsProps) => {
  const { handleMobileToggle } = useContext(MobileContext);
  const { handleFiltersSetList } = useContext(FiltersContext);
  const { handleSetListName, handleSetListId } = useContext(ListsContext);
  const list = filterData.filter((list) => list.name === `${name}`)[0];

  return (
    <button
      className="list-card"
      onClick={() => {
        handleFiltersSetList(name);
        handleMobileToggle();
        handleSetListName(list.name);
        handleSetListId(0);
      }}
      key={list._id}
    >
      <div className="list-card-row">
        {icon === 'list' ? (
          <ListIcon color={list.color} />
        ) : (
          <FlagIcon isFlagged={true} />
        )}
        <span data-testid="card-quantity" className="headline-5">
          {quantity}
        </span>
      </div>
      <div className="list-card-row">
        <span data-testid="card-text" className="body-2">
          {list.name}
        </span>
      </div>
    </button>
  );
};
