import { render, screen } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ListItemInfo } from '../components/ListItemInfo';

let testProps = {
  isEditing: true,
  handleEditToggle: () => {},
  id: 1123,
};
const setup = () =>
  render(
    <QueryClientProvider client={new QueryClient()}>
      <ListItemInfo
        isEditing={testProps.isEditing}
        handleEditToggle={testProps.handleEditToggle}
        id={testProps.id}
      />
    </QueryClientProvider>
  );

describe('ListItemInfo Component Testing', () => {
  test('Component container has class todos-item-info-edit when isEditing is true', () => {
    testProps = { ...testProps, isEditing: true };
    setup();
    const classes = screen
      .getByRole('contentinfo')
      .getAttribute('class')
      ?.split(' ')
      .includes('todos-item-info-edit');
    expect(classes).toBeTruthy();
  });
  test('Component container has class todos-item-info-noEdit when isEditing is false', () => {
    testProps = { ...testProps, isEditing: false };
    setup();
    const classes = screen
      .getByRole('contentinfo')
      .getAttribute('class')
      ?.split(' ')
      .includes('todos-item-info-noEdit');
    expect(classes).toBeTruthy();
  });
});
