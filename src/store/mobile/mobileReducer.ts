import { MobileInitialState } from '../../types/store';
import { MOBILE_CONST } from './mobileConstants';

export const mobileInitialState: MobileInitialState = {
  isMobileList: true,
};

export const mobileReducer = (state: MobileInitialState, action: any) => {
  const { type, payload } = action;

  switch (type) {
    case MOBILE_CONST.TOGGLE:
      return {
        ...state,
        isMobileList: payload,
      };
    default:
      return state;
  }
};
