import { useContext } from 'react';
import { TagsContext } from '../../../store/TagsContext';
import ErrorMessage from '../../errors/components/ErrorMessage';
import '../styles/tagContainer.css';
import { ListTags } from './ListTags';

type TagsContainerProps = {};

export const TagsContainer = ({}: TagsContainerProps) => {
  const { tags } = useContext(TagsContext);

  return (
    <div className="tags-container">
      <p className="caption-1">Tags</p>
      {tags.length ? (
        <ListTags tags={tags} />
      ) : (
        <ErrorMessage height="24px">
          <p className="body-1">No Tags Available</p>
        </ErrorMessage>
      )}
    </div>
  );
};
