import { createContext, useReducer } from 'react';
import { MOBILE_CONST } from './mobile/mobileConstants';
import { mobileInitialState, mobileReducer } from './mobile/mobileReducer';

export const MobileContext = createContext({
  isMobileList: true,
  handleMobileToggle: () => {},
});

type MobileProviderProps = {
  children: any;
};

export const MobileProvider = ({ children }: MobileProviderProps) => {
  const [state, dispatch] = useReducer(mobileReducer, mobileInitialState);

  // actions
  const handleMobileToggle = () => {
    dispatch({
      type: MOBILE_CONST.TOGGLE,
      payload: !state.isMobileList,
    });
  };

  const value = {
    isMobileList: state.isMobileList,
    handleMobileToggle,
  };
  return (
    <MobileContext.Provider value={value}>{children}</MobileContext.Provider>
  );
};
