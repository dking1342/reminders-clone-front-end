import HttpStatusCode from '../../enums/httpStatusCode';
import { ApiAction, Res } from '../../types/api';
import { Tag } from '../../types/tags';

let tagResponse: Res<Tag> = {
  timestamp: new Date(Date.now()),
  status: HttpStatusCode.NOT_FOUND,
};

export const fetchTags = async (
  url: string,
  method?: ApiAction,
  body?: Tag
) => {
  try {
    let response: Response;

    if ((method && body) || method === 'DELETE') {
      response = await fetch(url, {
        method,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(body),
      });
    } else {
      response = await fetch(url);
    }

    if (response.ok) {
      const data: Res<Tag> = await response.json();
      return data;
    } else {
      tagResponse = {
        ...tagResponse,
        error: 'Network error',
      };
      return tagResponse;
    }
  } catch (error) {
    const e = error as Error;
    tagResponse = {
      ...tagResponse,
      error: e.message,
    };
    return tagResponse;
  }
};
