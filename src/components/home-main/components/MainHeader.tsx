import '../styles/mainHeader.css';
import MainHeaderAddButton from './MainHeaderAddButton';
import MainHeaderSubTitle from './MainHeaderSubTitle';
import MainHeaderTitle from './MainHeaderTitle';

type Props = {};

const MainHeader = ({}: Props) => {
  return (
    <header>
      <MainHeaderAddButton />
      <MainHeaderTitle />
      <MainHeaderSubTitle />
    </header>
  );
};

export default MainHeader;
