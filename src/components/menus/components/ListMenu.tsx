import { useContext, useEffect } from 'react';
import { useMutation } from 'react-query';
import {
  listApiPostUrl,
  listApiPutUrl,
} from '../../../api/lists/constants/urls';
import { fetchLists } from '../../../api/lists/fetchRequests';
import { FiltersContext } from '../../../store/FiltersContext';
import { ListsContext } from '../../../store/ListsContext';
import { MenusContext } from '../../../store/MenusContext';
import { TodosContext } from '../../../store/TodosContext';
import { List } from '../../../types/lists';
import { MenuCancelButton } from '../../UI/components/MenuCancelButton';
import { MenuOkButton } from '../../UI/components/MenuOkButton';
import '../styles/menus.css';
import { FormGroupColorPicker } from './FormGroupColorPicker';
import { FormGroupInputText } from './FormGroupInputText';

type ListMenuProps = {};

export const ListMenu = ({}: ListMenuProps) => {
  // context
  const {
    handleAddList,
    handleUpdateList,
    handleSetListName,
    handleSetListId,
    listName,
    lists,
  } = useContext(ListsContext);
  const { handleUpdateTodosList } = useContext(TodosContext);
  const { handleFiltersSetList } = useContext(FiltersContext);
  const {
    menus,
    listMenuFormValues,
    handleMenuListSetIsOpen,
    handleGetListMenuFormValues,
    handleSetListMenuFormValues,
    handleResetListMenuFormValues,
  } = useContext(MenusContext);

  // dom
  const form = document.querySelector('form') as HTMLFormElement;

  // mutation methods
  const postMutation = useMutation((list: List) => {
    return fetchLists(listApiPostUrl, menus.list.state, list);
  });
  const postResponse = async () => {
    try {
      const { data, status } = await postMutation.mutateAsync(
        listMenuFormValues
      );
      if (status === 201 && data) {
        const list: List = data as List;
        handleAddList(list);
        handleFiltersSetList(list.name);
        handleSetListName(list.name);
        handleSetListId(list._id!);
      } else {
        alert('Unable to save the list. Please try again');
      }
    } catch (error) {
      const err = error as Error;
      console.error({ error: err.message });
    }
  };
  const putMutation = useMutation(
    (list: List) => {
      const url = listApiPutUrl(listMenuFormValues._id);
      return fetchLists(url, menus.list.state, list);
    },
    {
      onSuccess: (data, variables) => {
        if (data.status === 201) {
          handleFiltersSetList(variables.name);
          handleSetListName(variables.name);
          handleSetListId(variables._id!);
          handleUpdateList(variables);
          handleUpdateTodosList({
            id: variables._id!,
            name: variables.name,
          });
        } else {
          alert('Unable to update the list. Please try again');
        }
      },
    }
  );

  // form methods
  const handleFormChange = (e: any, key: string) => {
    handleSetListMenuFormValues(e.target.value, key);
  };
  const handleSubmit = async () => {
    if (menus.list.state === 'POST') {
      await postResponse();
    } else {
      putMutation.mutate(listMenuFormValues);
    }
    handleResetListMenuFormValues();
    form.reset();
    handleMenuListSetIsOpen(false);
  };

  // useEffects
  useEffect(() => {
    if (menus.list.isOpen) {
      menus.list.state === 'PUT' &&
        handleGetListMenuFormValues(listName.id, lists);
    }
  }, [menus.list.isOpen]);

  return (
    <dialog id="listMenuDialog">
      <form method="dialog">
        <div>
          <p className="subtitle-2">
            {menus.list.state === 'POST' ? 'New List' : 'Update List'}
          </p>
        </div>
        <FormGroupInputText
          name="name"
          title="Name"
          handleChange={handleFormChange}
          value={listMenuFormValues.name}
        />
        <FormGroupColorPicker handleChange={handleFormChange} />
        <div className="form-group form-submit">
          <MenuCancelButton form={form} formType="list" />
          <MenuOkButton onClick={handleSubmit} />
        </div>
      </form>
    </dialog>
  );
};
