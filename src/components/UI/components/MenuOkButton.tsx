import '../styles/menuOkButton.css';

type MenuOkButtonProps = {
  onClick: () => void;
};

export const MenuOkButton = ({ onClick }: MenuOkButtonProps) => {
  return (
    <button className="menu-button caption-2" value="default" onClick={onClick}>
      OK
    </button>
  );
};
