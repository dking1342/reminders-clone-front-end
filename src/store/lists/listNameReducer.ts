import { LISTS_CONSTANTS } from './listsConstants';

export const listNameReducer = (
  state: { name: string; id: string | number },
  action: { type: string; payload: any }
) => {
  const { type, payload } = action;

  switch (type) {
    case LISTS_CONSTANTS.LIST_SET_ITEM_NAME:
      const listName: string = payload;
      state.name = listName;
      return state;

    case LISTS_CONSTANTS.LIST_SET_ITEM_ID:
      const listId: number = payload;
      state.id = listId;
      return state;

    default:
      return state;
  }
};
