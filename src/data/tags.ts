import { Tag } from '../types/tags';

export const tagData: Tag[] = [
  {
    _id: 1,
    name: '#fun',
    isSelected: false,
  },
  {
    _id: 2,
    name: '#jobs',
    isSelected: false,
  },
  {
    _id: 3,
    name: '#career',
    isSelected: false,
  },
];
