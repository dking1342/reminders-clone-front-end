import { useState } from 'react';
import { List } from '../../../types/lists';
import '../styles/listRow.css';
import { ListRowBody } from './ListRowBody';
import { ListRowEdit } from './ListRowEdit';

type ListRowProps = {
  list: List;
  quantity: number;
};

export const ListRow = ({ list, quantity }: ListRowProps) => {
  const [isEditOpen, setIsEditOpen] = useState(false);

  const handleEditOpenToggle = () => {
    setIsEditOpen(!isEditOpen);
  };

  return (
    <div
      data-testid="listRow"
      className={`${
        isEditOpen
          ? 'sidebar-list-row-container-open'
          : 'sidebar-list-row-container'
      }`}
    >
      <ListRowBody quantity={quantity} list={list} />
      <ListRowEdit id={list._id} handleEditOpenToggle={handleEditOpenToggle} />
    </div>
  );
};
