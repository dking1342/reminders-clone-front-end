import '../styles/forms.css';

type FormGroupInputCheckboxProps = {
  name: string;
  title: string;
  handleChange: (e: any, key: string) => void;
  value: boolean;
  checked: boolean;
};

export const FormGroupInputCheckbox = ({
  name,
  title,
  handleChange,
  checked,
  value,
}: FormGroupInputCheckboxProps) => {
  return (
    <div className="form-group caption-2">
      <label htmlFor={name}>{title}</label>
      <input
        type="checkbox"
        name={name}
        className="caption-2 form-input form-input-checkbox"
        onChange={(e) => handleChange(e, name)}
        checked={checked}
        value={value.toString()}
      />
    </div>
  );
};
