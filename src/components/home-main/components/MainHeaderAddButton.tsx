import { useContext } from 'react';
import { ListsContext } from '../../../store/ListsContext';
import { AddIcon } from '../../UI/components/AddIcon';
import { DialogButton } from '../../UI/components/DialogButton';
import '../styles/mainHeaderAddButton.css';

type Props = {};

const MainHeaderAddButton = ({}: Props) => {
  const { listName } = useContext(ListsContext);
  return (
    <div className="main-header-add-container" role={'contentinfo'}>
      {listName.name === 'All' || listName.name === 'Flagged' ? (
        <></>
      ) : (
        <DialogButton selector="#todoMenuDialog" action="POST">
          <AddIcon />
        </DialogButton>
      )}
    </div>
  );
};

export default MainHeaderAddButton;
