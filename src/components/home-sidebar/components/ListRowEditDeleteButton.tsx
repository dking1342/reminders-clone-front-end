import { useContext } from 'react';
import { useMutation } from 'react-query';
import { listApiDeleteUrl } from '../../../api/lists/constants/urls';
import { fetchLists } from '../../../api/lists/fetchRequests';
import { todoApiDeleteUrl } from '../../../api/todos/constants/urls';
import { fetchTodos } from '../../../api/todos/fetchRequest';
import { FiltersContext } from '../../../store/FiltersContext';
import { ListsContext } from '../../../store/ListsContext';
import { TodosContext } from '../../../store/TodosContext';
import { List } from '../../../types/lists';
import { Todo } from '../../../types/todos';
import { DeleteIcon } from '../../UI/components/DeleteIcon';

type ListRowEditDeleteButtonProps = {
  id: string | number | undefined;
  handleEditOpenToggle: () => void;
};

export const ListRowEditDeleteButton = ({
  id,
  handleEditOpenToggle,
}: ListRowEditDeleteButtonProps) => {
  // context
  const {
    handleDeleteList,
    handleSetListName,
    handleSetListId,
    lists,
    listName,
  } = useContext(ListsContext);
  const { todos, handleDeleteTodosList } = useContext(TodosContext);
  const { handleFiltersSetList } = useContext(FiltersContext);

  const handleSetGlobalList = () => {
    // let list = lists.find((list) => list.name === 'All')!;
    handleSetListName('All');
    handleSetListId('');
    handleFiltersSetList('All');
  };

  const handleBulkTodoDelete = () => {
    const todosToDelete: Todo[] = todos.filter(
      (todo) => todo.list._id === listName.id
    );
    todosToDelete.forEach((todo) => {
      deleteTodoMutation.mutate(todo._id);
    });
  };

  // mutations
  const deleteTodoMutation = useMutation((id: string | number | undefined) => {
    const url = todoApiDeleteUrl(id);
    return fetchTodos(url, 'DELETE');
  });

  const mutation = useMutation(
    (id: string | number | undefined) => {
      const url = listApiDeleteUrl(id);
      return fetchLists(url, 'DELETE');
    },
    {
      onSuccess: (data, variables) => {
        if (data.status === 204) {
          handleDeleteList(variables);
          handleEditOpenToggle();

          // remove list from todos state
          handleDeleteTodosList(variables);

          // set global list state
          handleSetGlobalList();
        }
      },
      onError: () => {
        alert('Unable to delete. Please try again');
      },
    }
  );

  const handleDeleteClick = () => {
    const userConfirmation = confirm(
      'Deleting list will delete all todos. Are you sure you want to continue?'
    );

    if (userConfirmation) {
      handleBulkTodoDelete();
      mutation.mutate(id);
    }
  };

  return (
    <button onClick={handleDeleteClick}>
      <DeleteIcon size="small" />
    </button>
  );
};
