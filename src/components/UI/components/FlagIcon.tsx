import { useState, useEffect } from 'react';

type FlagIconProps = {
  isFlagged?: boolean;
};

export const FlagIcon = ({ isFlagged }: FlagIconProps) => {
  const [fill, setFill] = useState('transparent');

  useEffect(() => {
    switch (isFlagged) {
      case false:
        setFill('transparent');
        break;
      case true:
        setFill('orange');
        break;
      default:
        setFill('transparent');
        break;
    }
  }, [isFlagged]);

  return (
    <svg
      viewBox="0 0 50 50"
      xmlns="http://www.w3.org/2000/svg"
      width={25}
      height={25}
      id="flagIcon"
      data-testid="flagIcon"
    >
      <g stroke="orange" strokeWidth={2} strokeLinecap="round">
        <line x1="5" y1="5" x2="5" y2="40" />
        <path
          d="M 5 7 c0,0 3,7 15,0 c0,0 5,-5 17,3 l0,0 0,14 c0,0 -5,-5 -12,-2 c0,0 -8,5 -18,2"
          fill={fill}
          strokeWidth={2}
          data-testid="flagIcon-inside"
        />
      </g>
    </svg>
  );
};
