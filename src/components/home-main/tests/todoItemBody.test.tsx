import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, screen } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { tagData } from '../../../data/tags';
import { Tag } from '../../../types/tags';
import { TodoItemBody } from '../components/TodoItemBody';

type Props = {
  name: string;
  tags: Tag[];
  id: number;
  isFlaggedIcon: boolean;
  isDone: boolean;
};
let props: Props = {
  name: '',
  tags: tagData,
  id: 0,
  isFlaggedIcon: false,
  isDone: false,
};

const setup = () =>
  render(
    <QueryClientProvider client={new QueryClient()}>
      <TodoItemBody
        name={props.name}
        tags={props.tags}
        id={props.id}
        isFlaggedIcon={props.isFlaggedIcon}
        isDone={props.isDone}
      />
    </QueryClientProvider>
  );

describe('TodoItemBody Component Tests', () => {
  test('Span element has class isDone when the prop isDone is true', () => {
    props = { ...props, isDone: true };
    const className = 'isDone';
    setup();
    const classes = screen
      .getByTestId('todoItemBody-span')
      .getAttribute('class')
      ?.split(' ')
      .includes(className);
    expect(classes).toBeTruthy();
  });
  test('Span element does not have the class isDone when the prop isDone is false', () => {
    props = { ...props, isDone: false };
    const className = 'isDone';
    setup();
    const classes = screen
      .getByTestId('todoItemBody-span')
      .getAttribute('class')
      ?.split(' ')
      .includes(className);
    expect(classes).toBe(false);
  });
  test('Span element has text with the name props', () => {
    props = { ...props, name: 'Test item' };
    setup();
    const textContent = screen.getByTestId('todoItemBody-span').textContent;
    expect(textContent).toBe(props.name);
  });
  test('Flag icon button is present when prop isDone is false', () => {
    props = { ...props, isDone: false };
    setup();
    const btn = screen.getByTestId('listItemFlag-button');
    expect(btn).toBeInTheDocument;
  });
  test('Flag has orange fill when isFlaggedIcon prop is true', () => {
    props = { ...props, isDone: false, isFlaggedIcon: true };
    const color = 'orange';
    setup();
    const flagIconInside = screen
      .getByTestId('flagIcon-inside')
      .getAttribute('fill');
    expect(flagIconInside).toBe(color);
  });
  test('Flag has transparent fill when isFlaggedIcon prop is false', () => {
    props = { ...props, isDone: false, isFlaggedIcon: false };
    const color = 'transparent';
    setup();
    const flagIconInside = screen
      .getByTestId('flagIcon-inside')
      .getAttribute('fill');
    expect(flagIconInside).toBe(color);
  });
});
