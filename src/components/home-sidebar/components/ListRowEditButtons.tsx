import '../styles/listRowEditButtons.css';
import { ListRowEditDeleteButton } from './ListRowEditDeleteButton';
import { ListRowEditUpdateButton } from './ListRowEditUpdateButton';

type ListRowEditButtonsProps = {
  id: string | number | undefined;
  handleEditOpenToggle: () => void;
};

export const ListRowEditButtons = ({
  id,
  handleEditOpenToggle,
}: ListRowEditButtonsProps) => {
  return (
    <div className="list-row-edit-btns-container">
      <ListRowEditUpdateButton
        id={id}
        handleEditOpenToggle={handleEditOpenToggle}
      />
      <ListRowEditDeleteButton
        id={id}
        handleEditOpenToggle={handleEditOpenToggle}
      />
    </div>
  );
};
