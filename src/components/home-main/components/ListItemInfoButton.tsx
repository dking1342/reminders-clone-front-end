import { InfoIcon } from '../../UI/components/InfoIcon';
import '../styles/listItemInfoButton.css';

type ListItemInfoButtonProps = {
  handleEditToggle: () => void;
};

export const ListItemInfoButton = ({
  handleEditToggle,
}: ListItemInfoButtonProps) => {
  return (
    <button
      data-testid="listItemInfoButton"
      role={'button'}
      className="todos-item-info-button"
      onClick={handleEditToggle}
    >
      <InfoIcon />
    </button>
  );
};
