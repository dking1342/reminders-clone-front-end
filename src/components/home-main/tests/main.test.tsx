import { screen, render } from '@testing-library/react';
import { ReactElement, JSXElementConstructor } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { MobileContext } from '../../../store/MobileContext';
import Main from '../components/Main';

const customRender = (
  ui: ReactElement<any, string | JSXElementConstructor<any>>,
  { providerProps, ...renderOptions }: any
) => {
  return render(
    <QueryClientProvider client={new QueryClient()}>
      <MobileContext.Provider value={providerProps}>
        {ui}
      </MobileContext.Provider>
    </QueryClientProvider>,
    renderOptions
  );
};

describe('Main Component Testing', () => {
  test('Component has main-open class when isMobileList is false', () => {
    const providerProps = {
      ...MobileContext,
      isMobileList: false,
    };
    customRender(<Main />, { providerProps });
    const section = screen
      .getByRole('main')
      .getAttribute('class')
      ?.split(' ')
      .includes('main-open');
    expect(section).toBeTruthy();
  });
  test('Component does not have main-open class when isMobileList is true', () => {
    const providerProps = {
      ...MobileContext,
      isMobileList: true,
    };
    customRender(<Main />, { providerProps });
    const section = screen
      .getByRole('main')
      .getAttribute('class')
      ?.split(' ')
      .includes('main-open');
    expect(section).toBeFalsy();
  });
});
