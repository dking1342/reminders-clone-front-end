type EditIconsProps = {
  size: 'small' | 'large';
};

export const EditIcons = ({ size = 'large' }: EditIconsProps) => {
  const circle = '25';
  let dimension = '20';
  size === 'small' ? (dimension = '15') : (dimension = '20');

  return (
    <svg
      viewBox="0 0 50 50"
      xmlns="http://www.w3.org/2000/svg"
      width={dimension}
      height={dimension}
      fill="transparent"
    >
      <circle
        cx={circle}
        cy={circle}
        r={circle}
        stroke="#30B4FF"
        strokeWidth={2}
      />
      <g fill={'#ffffff'} stroke="#30B4FF">
        <path
          strokeWidth={3}
          fill="transparent"
          d="M 10 40 l0,0 12,-4 l0,0 -10,-8,l0,0 -2,12"
        />
        <path
          strokeWidth={3}
          fill="transparent"
          d="M 22 36 l0,0 20,-20 l0,0 -10,-8 l0,0 -20,20"
        />
      </g>
    </svg>
  );
};
