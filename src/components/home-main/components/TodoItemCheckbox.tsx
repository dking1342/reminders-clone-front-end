import { useContext } from 'react';
import { useMutation } from 'react-query';
import { todoApiPutUrl } from '../../../api/todos/constants/urls';
import { fetchTodos } from '../../../api/todos/fetchRequest';
import { TodosContext } from '../../../store/TodosContext';
import { Todo } from '../../../types/todos';
import '../styles/todoItemCheckbox.css';

type TodoItemCheckboxProps = {
  todo: Todo;
};

export const TodoItemCheckbox = ({ todo }: TodoItemCheckboxProps) => {
  const { handleUpdateTodoDone } = useContext(TodosContext);

  const todoMutation = useMutation(
    (id: string | number) => {
      const url = todoApiPutUrl(id);
      return fetchTodos(url, 'PUT', { ...todo, isDone: !todo.isDone });
    },
    {
      onSuccess: (data) => {
        if (data.status === 201) {
          handleUpdateTodoDone(todo._id);
        }
      },
    }
  );
  const handleChange = () => {
    todoMutation.mutate(todo._id!);
  };
  return (
    <div className="todos-item-checkbox">
      <input
        data-testid="todoItemCheckbox"
        aria-checked={todo.isDone}
        type="checkbox"
        name="todo"
        id="isDone"
        role={'checkbox'}
        checked={todo.isDone}
        value={String(todo.isDone)}
        onChange={handleChange}
      />
    </div>
  );
};
