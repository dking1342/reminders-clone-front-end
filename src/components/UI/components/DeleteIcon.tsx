type DeleteIconProps = {
  size: 'small' | 'large';
};

export const DeleteIcon = ({ size = 'large' }: DeleteIconProps) => {
  let circle = '25';
  let dimension = '25';
  let lineStart = '10';
  let lineEnd = '40';
  let strokeWidth = 5;

  if (size === 'small') {
    dimension = '22';
    strokeWidth = 4;
  }

  return (
    <svg
      viewBox="0 0 50 50"
      xmlns="http://www.w3.org/2000/svg"
      width={dimension}
      height={dimension}
    >
      <circle cx={circle} cy={circle} r={circle} fill="#ff0000" />
      <line
        x1={lineStart}
        x2={lineEnd}
        y1={circle}
        y2={circle}
        strokeWidth={strokeWidth}
        stroke="#fff"
        strokeLinecap="round"
      />
    </svg>
  );
};
