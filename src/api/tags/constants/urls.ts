type IdType = string | number | undefined;

const baseUrl = 'http://157.230.217.83:80/api/tags';

export const tagApiGetAllUrl = `${baseUrl}/get/all`;
export const tagApiPostUrl = `${baseUrl}/create`;
export const tagApiGetOneUrl = (id: IdType) => {
  return `${baseUrl}/get/one/${id}`;
};
export const tagApiPutUrl = (id: IdType) => {
  return `${baseUrl}/update/${id}`;
};
export const tagApiDeleteUrl = (id: IdType) => {
  return `${baseUrl}/delete/${id}`;
};
