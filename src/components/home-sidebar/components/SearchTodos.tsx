import '../styles/searchTodos.css';
import { SearchIcon } from '../../UI/components/SearchIcon';
import { useContext } from 'react';
import { FiltersContext } from '../../../store/FiltersContext';

type SearchTodosProps = {};

const SearchTodos = ({}: SearchTodosProps) => {
  const { filter, handleFiltersSetSearch } = useContext(FiltersContext);

  return (
    <div className="sidebar-row search-row">
      <SearchIcon />
      <input
        data-testid="search"
        type="text"
        name="search"
        id="search-input"
        placeholder="Search"
        className="body-1"
        value={filter.search}
        onChange={(e: any) => {
          handleFiltersSetSearch(e.target.value);
        }}
      />
    </div>
  );
};

export default SearchTodos;
