import HttpStatusCode from '../../enums/httpStatusCode';
import { ApiAction, Res } from '../../types/api';
import { List } from '../../types/lists';

let listResponse: Res<List> = {
  timestamp: new Date(Date.now()),
  status: HttpStatusCode.NOT_FOUND,
};

export const fetchLists = async (
  url: string,
  method?: ApiAction,
  body?: List
) => {
  try {
    let response: Response;

    if ((method && body) || method === 'DELETE') {
      response = await fetch(url, {
        method,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(body),
      });
    } else {
      response = await fetch(url);
    }

    if (response.ok) {
      const data: Res<List> = await response.json();
      return data;
    } else {
      listResponse = {
        ...listResponse,
        error: 'Network error',
      };
      return listResponse;
    }
  } catch (error) {
    const e = error as Error;
    listResponse = {
      ...listResponse,
      error: e.message,
    };
    return listResponse;
  }
};
