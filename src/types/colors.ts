export type ListColors = {
  id: number;
  color: '200' | '300' | '400' | '500' | '600' | '700' | '800';
  hexValue:
    | '#F4CA46'
    | '#E9883E'
    | '#E25641'
    | '#D73B4A'
    | '#B33D35'
    | '#B22D4D'
    | '#902056';
  isSelected: boolean;
};

export type ColorNumber = {
  color: '200' | '300' | '400' | '500' | '600' | '700' | '800';
};

export enum ColorPick {
  'Two' = 200,
  'Three' = 300,
  'Four' = 400,
  'Five' = 500,
  'Six' = 600,
  'Seven' = 700,
  'Eight' = 800,
}
