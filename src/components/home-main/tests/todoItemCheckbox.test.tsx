import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, screen } from '@testing-library/react';
import { JSXElementConstructor, ReactElement } from 'react';
import { Query, QueryClient, QueryClientProvider } from 'react-query';
import { listData } from '../../../data/lists';
import { todosData } from '../../../data/todos';
import { TodosContext } from '../../../store/TodosContext';
import { Todo } from '../../../types/todos';
import { TodoItemCheckbox } from '../components/TodoItemCheckbox';

type Props = {
  todo: Todo;
};
let props: Props = {
  todo: {
    _id: 1123,
    name: 'test',
    tags: [],
    isFlagged: false,
    isDone: false,
    list: listData[0],
  } as Todo,
};
const setup = () =>
  render(
    <QueryClientProvider client={new QueryClient()}>
      <TodoItemCheckbox todo={props.todo} />
    </QueryClientProvider>
  );

const customRender = (
  ui: ReactElement<any, string | JSXElementConstructor<any>>,
  { providerProps, ...renderOptions }: any
) => {
  return render(
    <QueryClientProvider client={new QueryClient()}>
      <TodosContext.Provider value={providerProps}>{ui}</TodosContext.Provider>
    </QueryClientProvider>,
    renderOptions
  );
};
describe('TodoItemCheckbox Component Tests', () => {
  test('Checkbox is checked when isDone prop is true', () => {
    props = { ...props, todo: { ...props.todo, isDone: true } };
    setup();
    const checkbox = screen
      .getByTestId('todoItemCheckbox')
      .getAttribute('aria-checked');
    expect(checkbox).toBe(String(props.todo.isDone));
  });
  test('Checkbox will not be checked when isDone prop is false', () => {
    props = { ...props, todo: { ...props.todo, isDone: false } };
    setup();
    const checkbox = screen
      .getByTestId('todoItemCheckbox')
      .getAttribute('aria-checked');
    expect(checkbox).toBe(String(props.todo.isDone));
  });
  test('Checkbox will not be checked when isDone is false', () => {
    props = { ...props, todo: { ...props.todo, isDone: false } };
    const providerProps = {
      ...TodosContext,
    };

    customRender(
      <QueryClientProvider client={new QueryClient()}>
        <TodoItemCheckbox todo={props.todo} />
      </QueryClientProvider>,
      {
        providerProps,
      }
    );
    const element = screen.getByTestId('todoItemCheckbox') as HTMLInputElement;
    expect(element).toBeInTheDocument();
    expect(element).not.toBeChecked();
  });
  test('Checkbox will be checked when isDone is true', () => {
    props = { ...props, todo: { ...props.todo, isDone: true } };
    const providerProps = {
      ...TodosContext,
    };

    customRender(
      <QueryClientProvider client={new QueryClient()}>
        <TodoItemCheckbox todo={props.todo} />
      </QueryClientProvider>,
      {
        providerProps,
      }
    );
    const element = screen.getByTestId('todoItemCheckbox') as HTMLInputElement;
    expect(element).toBeInTheDocument();
    expect(element).toBeChecked();
  });
});
