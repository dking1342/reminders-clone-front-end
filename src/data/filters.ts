import { Tag } from '../types/tags';

export type FilterType = {
  search: string;
  list: string;
  tags: Tag[];
  isShowing: boolean;
};

export const filtersData: FilterType = {
  search: '',
  list: 'All',
  tags: [],
  isShowing: true,
};
