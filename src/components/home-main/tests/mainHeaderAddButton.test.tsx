import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { JSXElementConstructor, ReactElement, ReactFragment } from 'react';
import { ListsContext } from '../../../store/ListsContext';
import MainHeaderAddButton from '../components/MainHeaderAddButton';

const customRender = (
  ui: ReactElement<any, string | JSXElementConstructor<any>> | ReactFragment,
  { providerProps, ...renderOptions }: any
) => {
  return render(
    <ListsContext.Provider value={providerProps}>{ui}</ListsContext.Provider>,
    renderOptions
  );
};

describe('Conditional rendering', () => {
  test('Renders empty fragment with global state of listName.name === All', () => {
    const providerProps = {
      ...ListsContext,
      listName: { name: 'All', id: 0 },
    };
    customRender(<MainHeaderAddButton />, { providerProps });
    expect(screen.getByRole('contentinfo')).toBeEmptyDOMElement();
  });

  test('Renders empty fragment with global state of listName.name === Flagged', () => {
    const providerProps = {
      ...ListsContext,
      listName: { name: 'Flagged', id: 0 },
    };
    customRender(<MainHeaderAddButton />, { providerProps });
    expect(screen.getByRole('contentinfo')).toBeEmptyDOMElement();
  });

  test('Renders button element when listName.name === Chores', () => {
    const providerProps = {
      ...ListsContext,
      listName: { name: 'Chores', id: 0 },
    };
    customRender(<MainHeaderAddButton />, { providerProps });
    expect(screen.getByRole('button')).toBeInTheDocument();
  });
});
