type SearchIconProps = {};

export const SearchIcon = ({}: SearchIconProps) => {
  return (
    <svg
      viewBox="0 0 50 50"
      xmlns="http://www.w3.org/2000/svg"
      width={25}
      height={25}
    >
      <g fill={'#ffffff'} stroke="white">
        <circle
          cx="20"
          cy="20"
          r="15"
          fill="transparent"
          stroke="white"
          strokeWidth={3}
        />
        <line
          x1="32"
          y1="32"
          x2="45"
          y2="45"
          strokeWidth={3}
          strokeLinecap="round"
        />
      </g>
    </svg>
  );
};
