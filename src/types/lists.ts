export type List = {
  _id?: string | number;
  name: string;
  color: string;
  // color: '200' | '300' | '400' | '500' | '600' | '700' | '800';
  createdAt: number;
  updatedAt: number;
};
