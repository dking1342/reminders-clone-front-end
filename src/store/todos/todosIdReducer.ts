import { TODOS_CONSTANTS } from './todosConstants';

export const todosIdReducer = (
  state: string,
  action: { type: string; payload: any }
) => {
  const { type, payload } = action;

  switch (type) {
    case TODOS_CONSTANTS.TODO_SET_ID:
      let id: string = payload;
      state = id;
      return state;

    default:
      return state;
  }
};
