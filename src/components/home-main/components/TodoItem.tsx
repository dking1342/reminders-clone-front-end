import '../styles/todoItem.css';
import { useState, useEffect } from 'react';
import { Todo } from '../../../types/todos';
import { ListItemInfo } from './ListItemInfo';
import { TodoItemBody } from './TodoItemBody';
import { TodoItemCheckbox } from './TodoItemCheckbox';

type TodoItemProps = {
  todo: Todo;
};

export const TodoItem = ({ todo }: TodoItemProps) => {
  const { _id, name, tags, isFlagged, isDone } = todo;
  let [isFlaggedIcon, setIsFlaggedIcon] = useState(isFlagged);
  let [isEditing, setIsEditing] = useState(false);

  const handleEditToggle = () => {
    setIsEditing(!isEditing);
  };

  useEffect(() => {
    setIsFlaggedIcon(isFlagged);
  }, [isFlagged]);

  return (
    <div
      data-testid="todoItem-container"
      className={`todos-item-row ${
        isEditing ? 'todos-item-row-isEditing' : 'todos-items-row-notEditing'
      }`}
    >
      <TodoItemCheckbox todo={todo} />

      <TodoItemBody
        name={name}
        tags={tags}
        id={_id}
        isFlaggedIcon={isFlaggedIcon}
        isDone={isDone}
      />

      {!isDone && (
        <ListItemInfo
          isEditing={isEditing}
          handleEditToggle={handleEditToggle}
          id={_id}
        />
      )}
    </div>
  );
};
