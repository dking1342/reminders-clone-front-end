import { createContext, useContext, useEffect, useReducer } from 'react';
import { filtersData } from '../data/filters';
import { Filter } from '../types/filters';
import { Tag } from '../types/tags';
import { Todo } from '../types/todos';
import { FILTERS_CONSTANTS } from './filters/filterConstants';
import { filteredTodosReducer } from './filters/filteredTodosReducer';
import { filterReducer } from './filters/filterReducer';
import { ListsContext } from './ListsContext';
import { TodosContext } from './TodosContext';

export const filterInitialState: Filter = {
  search: '',
  list: 'All',
  tags: [],
  isShowing: true,
};
export const filteredTodosInitialState: Todo[] = [];

export const FiltersContext = createContext({
  filter: filterInitialState,
  filteredTodos: filteredTodosInitialState,
  handlePopulateFilter: (filter: Filter) => {},
  handleFiltersSetSearch: (search: string) => {},
  handleFiltersSetList: (list: string) => {},
  handleFiltersSetShowing: () => {},
  handleFiltersSetTags: (input: {
    id: string | number | undefined;
    tags: Tag[];
  }) => {},
});

type FilterProviderProps = {
  children: any;
};

export const FilterProvider = ({ children }: FilterProviderProps) => {
  const [state, dispatch] = useReducer(filterReducer, filterInitialState);
  const [todosState, todosDispatch] = useReducer(
    filteredTodosReducer,
    filteredTodosInitialState
  );
  const { todos } = useContext(TodosContext);
  const { lists } = useContext(ListsContext);

  // actions
  const handlePopulateFilter = (filter: Filter) => {
    dispatch({
      type: FILTERS_CONSTANTS.FILTER_POPULATE_ALL,
      payload: filter,
    });
  };
  const handleFiltersSetSearch = (search: string) => {
    dispatch({
      type: FILTERS_CONSTANTS.FILTER_SET_FILTERS_SEARCH,
      payload: search,
    });
  };
  const handleFiltersSetList = (list: string) => {
    dispatch({
      type: FILTERS_CONSTANTS.FILTER_SET_FILTERS_LIST,
      payload: list,
    });
  };
  const handleFiltersSetShowing = () => {
    dispatch({
      type: FILTERS_CONSTANTS.FILTER_SET_FILTERS_SHOWING,
      payload: null,
    });
  };
  const handleFiltersSetTags = (input: {
    id: string | number | undefined;
    tags: Tag[];
  }) => {
    dispatch({
      type: FILTERS_CONSTANTS.FILTER_SET_FILTERS_TAGS,
      payload: input,
    });
  };

  // filter helper methods
  const handleTodosFilterBySearch = (search: string, todos: Todo[]) => {
    return todos.filter((todo) =>
      todo.name.toLowerCase().includes(search.toLowerCase())
    );
  };
  const handleTodosFilterByList = (list: string, todos: Todo[]) => {
    if (list === 'All') {
      return todos;
    } else if (list === 'Flagged') {
      return todos.filter((todo) => todo.isFlagged);
    } else {
      return todos.filter(
        (todo) => todo.list.name.toLowerCase() === list.toLowerCase()
      );
    }
  };
  const handleTodosFilterByTags = (tags: Tag[], todos: Todo[]) => {
    return todos.reduce((acc: Todo[], val) => {
      tags.forEach((tag) => {
        if (val.tags.find((t) => t.name === tag.name)) {
          acc.find((t) => t._id === val._id) ? null : acc.push(val);
        }
      });
      return acc;
    }, []);
  };
  const handleTodosFilterByHidingDone = (isShowing: boolean, todos: Todo[]) => {
    return todos.filter((todo) => todo.isDone === isShowing);
  };
  const handleTodosFilter = (filter: Filter) => {
    let filteredResult = [];

    // list filter
    filteredResult = handleTodosFilterByList(filter.list, todos);

    // tags filter
    if (filter.tags.length) {
      filteredResult = handleTodosFilterByTags(filter.tags, filteredResult);
    }

    // search filter
    if (filter.search) {
      filteredResult = handleTodosFilterBySearch(filter.search, filteredResult);
    }

    // isShowing filter
    if (!filter.isShowing) {
      filteredResult = handleTodosFilterByHidingDone(
        filter.isShowing,
        filteredResult
      );
    }

    todosDispatch({
      type: FILTERS_CONSTANTS.FILTER_TODOS,
      payload: filteredResult,
    });
  };

  useEffect(() => {
    handleTodosFilter(state);
  }, [state, lists]);

  useEffect(() => {
    handleTodosFilter(state);
  }, [lists]);

  useEffect(() => {
    if (todos.length) {
      handleTodosFilter(state);
    }
  }, [todos]);

  useEffect(() => {
    handlePopulateFilter(filtersData);
  }, []);

  const value = {
    filter: state,
    search: state.search as string,
    list: state.list,
    tags: state.tags,
    isShowing: state.isShowing,
    filteredTodos: todosState,
    handlePopulateFilter,
    handleFiltersSetSearch,
    handleFiltersSetList,
    handleFiltersSetShowing,
    handleFiltersSetTags,
  };

  return (
    <FiltersContext.Provider value={value}>{children}</FiltersContext.Provider>
  );
};
