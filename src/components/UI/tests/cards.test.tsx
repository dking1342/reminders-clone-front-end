import '@testing-library/jest-dom/extend-expect';
import { screen, render } from '@testing-library/react';
import { ListCards } from '../components/Cards';

type Props = {
  name: string;
  quantity: number;
  icon: 'list' | 'flag';
};
let props: Props = {
  name: 'All',
  quantity: 5,
  icon: 'flag',
};

const setup = () =>
  render(
    <ListCards name={props.name} quantity={props.quantity} icon={props.icon} />
  );

describe('Cards Component Tests', () => {
  test('Quantity is showing 5', () => {
    const quantity = 5;
    setup();
    const element = screen.getByTestId('card-quantity');
    expect(element).toBeInTheDocument();
    expect(element.textContent).toBe(String(quantity));
  });
  test('Text is showing All', () => {
    const text = 'All';
    setup();
    const element = screen.getByTestId('card-text');
    expect(element).toBeInTheDocument();
    expect(element.textContent).toBe(text);
  });
  test('Flag icon is showing when icon prop is flag', () => {
    setup();
    const element = screen.getByTestId('flagIcon');
    expect(element).toBeInTheDocument();
  });
  test('Flag icon is not showing when icon prop is list', () => {
    const icon = 'list';
    props = { ...props, icon };
    setup();
    const element = screen.getByTestId('listIcon');
    expect(element).toBeInTheDocument();
  });
});
