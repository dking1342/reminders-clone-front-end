import { useContext } from 'react';
import { FiltersContext } from '../../../store/FiltersContext';
import { MenusContext } from '../../../store/MenusContext';
import { TagsContext } from '../../../store/TagsContext';
import { MenuAction } from '../../../types/menus';
import { Tag } from '../../../types/tags';
import '../styles/tagItem.css';

type TagItemProps = {
  tag: Tag;
  tags: Tag[];
  action?: MenuAction;
};

export const TagItem = ({ tag, action }: TagItemProps) => {
  const { _id, name, isSelected } = tag;
  const { handleFiltersSetTags } = useContext(FiltersContext);
  const { tags: allTags, handleToggleTag } = useContext(TagsContext);
  const { handleMenuTagClick, handleMenuTagDelete } = useContext(MenusContext);

  const handleClick = () => {
    switch (action) {
      case 'add':
        handleMenuTagClick(_id, allTags);
        break;
      case 'delete':
        handleMenuTagDelete(_id);
        break;
      default:
        handleToggleTag(_id);
        handleFiltersSetTags({ id: _id, tags: allTags });
        break;
    }
  };
  return (
    <button
      role={'button'}
      data-testid="tagItem-btn"
      className={`caption-1 tag-button ${
        isSelected ? 'tag-button-selected' : 'tag-button-unselected'
      }`}
      value="tags"
      onClick={handleClick}
    >
      {name}
    </button>
  );
};
