import { ReactNode } from 'react';
import '../styles/errorMessages.css';

type Props = {
  height: string;
  alignment?: 'left' | 'center' | 'right';
  children: ReactNode;
};

const ErrorMessage = ({ alignment = 'left', height, children }: Props) => {
  return (
    <div
      className="error-message"
      style={{ lineHeight: height, textAlign: alignment }}
    >
      {children}
    </div>
  );
};

export default ErrorMessage;
