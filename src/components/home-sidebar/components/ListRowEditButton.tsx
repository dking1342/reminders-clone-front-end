import { useContext } from 'react';
import { ListsContext } from '../../../store/ListsContext';
import { InfoIcon } from '../../UI/components/InfoIcon';
import '../styles/listRowEditButton.css';

type ListRowEditButtonProps = {
  id: string | number | undefined;
  handleEditOpenToggle: () => void;
};

export const ListRowEditButton = ({
  id,
  handleEditOpenToggle,
}: ListRowEditButtonProps) => {
  const { handleSetListId } = useContext(ListsContext);

  const handleClick = () => {
    handleSetListId(id);
    handleEditOpenToggle();
  };

  return (
    <button
      className="list-row-edit-info-button"
      data-testid="listRowEditButton"
      role={'button'}
      onClick={handleClick}
    >
      <InfoIcon />
    </button>
  );
};
