import { fireEvent, render, screen } from '@testing-library/react';
import { JSXElementConstructor, ReactElement } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { todosDataTest1 } from '../../../data/todos';
import { FiltersContext } from '../../../store/FiltersContext';
import { TodosContext } from '../../../store/TodosContext';
import MainHeaderSubTitle from '../components/MainHeaderSubTitle';

const customFilterRender = (
  ui: ReactElement<any, string | JSXElementConstructor<any>>,
  { providerProps, ...renderOptions }: any
) => {
  return render(
    <QueryClientProvider client={new QueryClient()}>
      <FiltersContext.Provider value={providerProps}>
        {ui}
      </FiltersContext.Provider>
    </QueryClientProvider>,
    renderOptions
  );
};
const customTodosRender = (
  ui: ReactElement<any, string | JSXElementConstructor<any>>,
  { providerProps, ...renderOptions }: any
) => {
  return render(
    <QueryClientProvider client={new QueryClient()}>
      <TodosContext.Provider value={providerProps}>{ui}</TodosContext.Provider>
    </QueryClientProvider>,
    renderOptions
  );
};

describe('MainHeaderSubTitle Component Testing', () => {
  test('Quantity of filtered todos that are done is showing 2 when the list is All', () => {
    const providerProps = {
      ...FiltersContext,
      filteredTodos: todosDataTest1,
      filter: {
        search: '',
        list: 'All',
        tags: [],
        isShowing: true,
      },
    };
    customFilterRender(<MainHeaderSubTitle />, { providerProps });
    const quantity = screen.getByTestId(
      'mainHeaderSubTitle-quantity'
    ).textContent;
    expect(quantity).toBe('2 Completed');
  });
  test('Quantity of filtered todos that are done is showing 1 when the list is Flagged', () => {
    const providerProps = {
      ...FiltersContext,
      filteredTodos: todosDataTest1,
      filter: {
        search: '',
        list: 'Flagged',
        tags: [],
        isShowing: true,
      },
    };
    customFilterRender(<MainHeaderSubTitle />, { providerProps });
    const quantity = providerProps.filteredTodos.filter(
      (todo) => todo.isDone && todo.isFlagged
    );
    expect(quantity.length).toBe(1);
  });
  test('Quantity of filtered todos that are done is showing 2 when the list is Chores', () => {
    const providerProps = {
      ...FiltersContext,
      filteredTodos: todosDataTest1,
      filter: {
        search: '',
        list: 'Chores',
        tags: [],
        isShowing: true,
      },
    };
    customFilterRender(<MainHeaderSubTitle />, { providerProps });
    const quantity = providerProps.filteredTodos.filter(
      (todo) => todo.isDone && todo.list.name === providerProps.filter.list
    );
    expect(quantity.length).toBe(2);
  });
  test('Quantity of filtered todos that are done is showing 0 when the list is Homework', () => {
    const providerProps = {
      ...FiltersContext,
      filteredTodos: todosDataTest1,
      filter: {
        search: '',
        list: 'Homework',
        tags: [],
        isShowing: true,
      },
    };
    customFilterRender(<MainHeaderSubTitle />, { providerProps });
    const quantity = providerProps.filteredTodos.filter(
      (todo) => todo.isDone && todo.list.name === providerProps.filter.list
    );
    expect(quantity.length).toBe(0);
  });
  test('Button clicked having the X function called', () => {
    const providerProps = {
      ...FiltersContext,
      filteredTodos: todosDataTest1,
      filter: {
        search: '',
        list: 'All',
        tags: [],
        isShowing: true,
      },
      handleFiltersSetShowing: () => {},
    };
    const handleClick = jest.spyOn(providerProps, 'handleFiltersSetShowing');
    customFilterRender(<MainHeaderSubTitle />, { providerProps });
    const btn = screen.getByRole('button', { name: /hide/i });
    fireEvent.click(btn);
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
  test('Button showing hide when isShowing state is true', () => {
    const providerProps = {
      ...FiltersContext,
      filteredTodos: todosDataTest1,
      filter: {
        search: '',
        list: 'All',
        tags: [],
        isShowing: true,
      },
    };
    customFilterRender(<MainHeaderSubTitle />, { providerProps });
    const btn = screen.getByRole('button', { name: /hide/i });
    expect(btn.textContent).toBe('Hide');
  });
  test('Button showing show when isShowing state is false', () => {
    const providerProps = {
      ...FiltersContext,
      filteredTodos: todosDataTest1,
      filter: {
        search: '',
        list: 'All',
        tags: [],
        isShowing: false,
      },
    };
    customFilterRender(<MainHeaderSubTitle />, { providerProps });
    const btn = screen.getByRole('button', { name: /show/i });
    expect(btn.textContent).toBe('Show');
  });
});
