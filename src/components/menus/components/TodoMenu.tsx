import { useContext, useEffect, useState } from 'react';
import { useMutation, useQuery } from 'react-query';
import { tagApiPostUrl } from '../../../api/tags/constants/urls';
import { fetchTags } from '../../../api/tags/fetchRequest';
import {
  todoApiGetOneUrl,
  todoApiPostUrl,
  todoApiPutUrl,
} from '../../../api/todos/constants/urls';
import { fetchMenuTodos, fetchTodos } from '../../../api/todos/fetchRequest';
import { ListsContext } from '../../../store/ListsContext';
import { MenusContext } from '../../../store/MenusContext';
import { TagsContext } from '../../../store/TagsContext';
import { TodosContext } from '../../../store/TodosContext';
import { MenuTodo } from '../../../types/menus';
import { Tag } from '../../../types/tags';
import { Todo } from '../../../types/todos';
import { ListTags } from '../../home-sidebar/components/ListTags';
import { MenuCancelButton } from '../../UI/components/MenuCancelButton';
import { MenuOkButton } from '../../UI/components/MenuOkButton';
import '../styles/menus.css';
import { FormGroupInputArray } from './FormGroupInputArray';
import { FormGroupInputCheckbox } from './FormGroupInputCheckbox';
import { FormGroupInputText } from './FormGroupInputText';
import { MenuTagOutput } from './MenuTagOutput';

type TodoMenuProps = {};

export const TodoMenu = ({}: TodoMenuProps) => {
  // context
  const { tagList, handleAddTagToAllTags } = useContext(TagsContext);
  const { todos, handleAddTodo, handleUpdateTodo } = useContext(TodosContext);
  const {
    menus,
    todoMenuFormValues,
    handleMenuTodoSetIsOpen,
    handleMenuReset,
    handleGetTodoMenuFormValues,
    handleSetTodoMenuFormValues,
    handleMenuSetTags,
    handleSetTodoMenuIdFormValues,
    handleResetTodoMenuFormValues,
    handleSetTodoMenuListFormValues,
    handleMenuTagUpdate,
  } = useContext(MenusContext);
  const { listName } = useContext(ListsContext);

  // useState
  const [todoId, setTodoId] = useState<string | number>('');
  const [isTagSaved, setIsTagSaved] = useState<boolean>(false);

  // dom element state
  const form = document.querySelector('form') as HTMLFormElement;
  const dialog = document.querySelector('#todoMenuDialog') as HTMLDialogElement;

  // form change state
  const handleFormChange = (e: any, key: string) => {
    handleSetTodoMenuFormValues(e.target.value, key, e.target.type);
  };

  // queries
  useQuery(['todo', todoId], async ({ queryKey }) => {
    try {
      if (queryKey[1]) {
        const url = todoApiGetOneUrl(queryKey[1]);
        let response = await fetchTodos(url);
        let todo = response.data as Todo;

        if (menus.todo.state === 'PUT') {
          handleUpdateTodo(todo);
        }
        if (menus.todo.state === 'POST') {
          handleAddTodo(todo);
        }
        handleMenuReset();
        handleResetTodoMenuFormValues();
        handleMenuTodoSetIsOpen(false);
        setTodoId('');
      }
    } catch (error) {
      console.log({ error });
    }
  });

  // mutations
  const postMutation = useMutation(
    (todo: MenuTodo) => {
      const url = todoApiPostUrl;
      return fetchMenuTodos(url, 'POST', todo);
    },
    {
      onSuccess: (data, variables) => {
        if (data.status === 201) {
          const savedTodo: Todo = data.data as Todo;
          const id = savedTodo._id!;
          setTodoId(id);
        }
      },
    }
  );
  const putMutation = useMutation(
    (todo: MenuTodo) => {
      const url = todoApiPutUrl(todoMenuFormValues._id!);
      return fetchMenuTodos(url, 'PUT', todo);
    },
    {
      onSuccess: (data, variables) => {
        if (data.status === 201) {
          setTodoId(variables._id!);
        }
      },
    }
  );
  const tagCreateMutation = useMutation(async (tag: Tag) => {
    const url = tagApiPostUrl;
    return await fetchTags(url, 'POST', tag);
  });

  // form submission
  const handleSubmit = () => {
    handleSaveTags();
  };

  const handleSaveTags = () => {
    const tagsToBeSaved = menus.selectedTags.filter(
      (tag) => typeof tag._id === 'number'
    );
    if (tagsToBeSaved.length) {
      tagsToBeSaved.forEach(async (tag) => {
        try {
          let { data, status } = await tagCreateMutation.mutateAsync({
            name: tag.name,
            isSelected: tag.isSelected,
          });
          if (status === 201) {
            const tag: Tag = data! as Tag;
            handleMenuTagUpdate(tag);
            handleAddTagToAllTags(tag);
          }
        } catch (error) {
          console.log({ error });
        }
      });
    }
    setIsTagSaved(true);
  };

  const handleSaveTodo = (tags: any) => {
    let values: MenuTodo = { ...todoMenuFormValues, tags };
    if (menus.todo.state === 'POST') {
      postMutation.mutate(values);
    } else {
      putMutation.mutate(values);
    }
  };

  //// useEffects
  // isOpen effects
  useEffect(() => {
    if (menus.todo.isOpen && !menus.submitted) {
      if (menus.todo.state === 'PUT' && todoMenuFormValues._id) {
        handleGetTodoMenuFormValues(todoMenuFormValues._id, todos);
        handleMenuSetTags(todoMenuFormValues._id, todos);
      } else {
        handleSetTodoMenuIdFormValues();
        handleSetTodoMenuListFormValues(listName.id);
      }
    }
  }, [menus.todo]);

  // saved tags
  useEffect(() => {
    if (isTagSaved) {
      const checkedIds = menus.selectedTags.every(
        (tag) => typeof tag._id === 'string'
      );
      const checkEmptyArray = menus.selectedTags.length === 0;
      const truthy1 = [
        checkedIds && isTagSaved && todoMenuFormValues.name.length > 2,
      ].every((value) => value === true);
      const truthy2 = [
        checkEmptyArray && isTagSaved && todoMenuFormValues.name.length > 2,
      ].every((value) => value === true);
      if (truthy1 || truthy2) {
        let tagsAll = menus.selectedTags;
        handleSaveTodo(tagsAll);
        setIsTagSaved(false);
      }
    }
  }, [menus.selectedTags, isTagSaved]);

  // dialog effects
  useEffect(() => {
    if (dialog) {
      dialog.addEventListener(
        'close',
        () => {
          if (dialog.returnValue === 'tags') {
            dialog.open = true;
          }
        },
        { once: false }
      );
    }
  }, [dialog]);

  return (
    <dialog id="todoMenuDialog">
      <form method="dialog">
        <p className="subtitle-2">
          {menus.todo.state === 'POST' ? 'New Todo' : 'Update Todo'}
        </p>
        <FormGroupInputText
          name="name"
          title="Name"
          handleChange={handleFormChange}
          value={todoMenuFormValues.name}
        />
        <FormGroupInputArray name="tags" title="Tags" value={menus.tagInput} />
        <ListTags tags={menus.filteredTags} action={'add'} />
        <MenuTagOutput />
        <FormGroupInputCheckbox
          name="isFlagged"
          title="Flag"
          handleChange={handleFormChange}
          value={todoMenuFormValues.isFlagged}
          checked={todoMenuFormValues.isFlagged}
        />
        <FormGroupInputCheckbox
          name="isDone"
          title="Done"
          handleChange={handleFormChange}
          value={todoMenuFormValues.isDone}
          checked={todoMenuFormValues.isDone}
        />
        {<div>{menus.errors && menus.errors}</div>}
        <div className="form-group form-submit">
          <MenuCancelButton form={form} formType="todo" />
          <MenuOkButton onClick={handleSubmit} />
        </div>
      </form>
    </dialog>
  );
};
