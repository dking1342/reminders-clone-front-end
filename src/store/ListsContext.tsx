import { createContext, useReducer } from 'react';
import { List } from '../types/lists';
import { listNameReducer } from './lists/listNameReducer';
import { LISTS_CONSTANTS } from './lists/listsConstants';
import { listsReducer } from './lists/listsReducer';

export const listsInitialState: List[] = [];
export const listNameInitialState: { name: string; id: string | number } = {
  name: 'All',
  id: 0,
};

export const ListsContext = createContext({
  lists: listsInitialState,
  listName: listNameInitialState,
  handlePopulateLists: (lists: List[]) => {},
  handleAddList: (list: List) => {},
  handleUpdateList: (list: List) => {},
  handleSetListName: (list: string) => {},
  handleSetListId: (id: string | number | undefined) => {},
  handleDeleteList: (id: string | number | undefined) => {},
});

type ListsProviderProps = {
  children: any;
};

export const ListsProvider = ({ children }: ListsProviderProps) => {
  const [state, dispatch] = useReducer(listsReducer, listsInitialState);
  const [listNameState, listNameDispatch] = useReducer(
    listNameReducer,
    listNameInitialState
  );

  // actions
  const handlePopulateLists = (lists: List[]) => {
    dispatch({
      type: LISTS_CONSTANTS.LIST_POPULATE,
      payload: lists,
    });
  };
  const handleAddList = async (list: List) => {
    dispatch({
      type: LISTS_CONSTANTS.LIST_ADD_ITEM,
      payload: list,
    });
  };
  const handleSetListName = (list: string) => {
    listNameDispatch({
      type: LISTS_CONSTANTS.LIST_SET_ITEM_NAME,
      payload: list,
    });
  };
  const handleSetListId = (id: string | number | undefined) => {
    listNameDispatch({
      type: LISTS_CONSTANTS.LIST_SET_ITEM_ID,
      payload: id,
    });
  };
  const handleUpdateList = (list: List) => {
    dispatch({
      type: LISTS_CONSTANTS.LIST_UPDATE_ITEM,
      payload: list,
    });
  };
  const handleDeleteList = (id: string | number | undefined) => {
    dispatch({
      type: LISTS_CONSTANTS.LIST_DELETE_ITEM,
      payload: id,
    });
  };

  const value = {
    lists: state,
    listName: listNameState,
    handlePopulateLists,
    handleAddList,
    handleSetListName,
    handleSetListId,
    handleUpdateList,
    handleDeleteList,
  };

  return (
    <ListsContext.Provider value={value}>{children}</ListsContext.Provider>
  );
};
