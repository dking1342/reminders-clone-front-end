import { ListColors } from '../types/colors';

const color: string = '200';
export const listColors: ListColors[] = [
  {
    id: 1,
    color: '200',
    hexValue: '#F4CA46',
    isSelected: color === '200' ? true : false,
  },
  {
    id: 2,
    color: '300',
    hexValue: '#E9883E',
    isSelected: color === '300' ? true : false,
  },
  {
    id: 3,
    color: '400',
    hexValue: '#E25641',
    isSelected: color === '400' ? true : false,
  },
  {
    id: 4,
    color: '500',
    hexValue: '#D73B4A',
    isSelected: color === '500' ? true : false,
  },
  {
    id: 5,
    color: '600',
    hexValue: '#B33D35',
    isSelected: color === '600' ? true : false,
  },
  {
    id: 6,
    color: '700',
    hexValue: '#B22D4D',
    isSelected: color === '700' ? true : false,
  },
  {
    id: 7,
    color: '800',
    hexValue: '#902056',
    isSelected: color === '800' ? true : false,
  },
];
