import Main from '../../components/home-main/components/Main';
import Sidebar from '../../components/home-sidebar/components/Sidebar';
import { ListMenu } from '../../components/menus/components/ListMenu';
import { TodoMenu } from '../../components/menus/components/TodoMenu';
import Mobile from '../../components/mobile/components/Mobile';
import '../styles/home.css';

type Props = {};

const Home = ({}: Props) => {
  return (
    <>
      <section className="container">
        <Sidebar />
        <Main />
      </section>
      <ListMenu />
      <TodoMenu />
      <Mobile />
    </>
  );
};

export default Home;
