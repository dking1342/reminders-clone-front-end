import HttpStatusCode from '../enums/httpStatusCode';

export type Res<T> = {
  timestamp: Date;
  count?: Number;
  status: HttpStatusCode;
  error?: String;
  message?: String;
  data?: T[] | T;
  developerMessage?: any;
};

export type ApiAction = 'GET' | 'POST' | 'PUT' | 'DELETE';
