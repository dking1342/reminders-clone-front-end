import '../styles/forms.css';

type FormGroupInputTextProps = {
  name: string;
  title: string;
  handleChange: (e: any, key: string) => void;
  value: string;
};

export const FormGroupInputText = ({
  name,
  title,
  handleChange,
  value,
}: FormGroupInputTextProps) => {
  return (
    <div className="form-group caption-2">
      <label htmlFor={name}>{title}</label>
      <input
        type="text"
        name={name}
        className="caption-2 form-input"
        onChange={(e) => handleChange(e, name)}
        value={value}
      />
    </div>
  );
};
