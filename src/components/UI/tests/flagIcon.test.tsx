import '@testing-library/jest-dom/extend-expect';
import { screen, render } from '@testing-library/react';
import { FlagIcon } from '../components/FlagIcon';

type Props = { isFlagged?: boolean };
let props: Props = { isFlagged: false };
const setup = () => render(<FlagIcon isFlagged={props.isFlagged} />);

const testFlagColor = (isFlagged: boolean, flagColor: string) => {
  props = { ...props, isFlagged };
  setup();
  const flag = screen.getByTestId('flagIcon-inside');
  expect(flag).toBeInTheDocument();
  expect(flag.getAttribute('fill')).toBe(flagColor);
};

describe('FlagIcon Component Tests', () => {
  test('Flag icon has inner color transparent when isFlagged prop is false', () => {
    testFlagColor(false, 'transparent');
  });
  test('Flag icon has inner color transparent when isFlagged prop is false', () => {
    testFlagColor(true, 'orange');
  });
});
