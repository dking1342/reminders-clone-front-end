import { listColors } from '../../../data/colors';
import { ListColors } from '../../../types/colors';
import { colorPicker } from '../../../utils/colorPicker';
import '../styles/forms.css';

type FormGroupColorPicker = {
  handleChange: (e: any, key: string) => void;
};

export const FormGroupColorPicker = ({
  handleChange,
}: FormGroupColorPicker) => {
  const colors = listColors;

  return (
    <div className="form-group caption-2">
      <label htmlFor="color">Color</label>
      <div className="form-color">
        {colors.map(({ id, color, hexValue }: ListColors) => (
          <label htmlFor={`${id}`} key={id}>
            <input
              type="radio"
              name="color"
              value={color}
              className="form-color-input"
              onChange={(e) => handleChange(e, 'color')}
            />
            <svg viewBox="0 0 30 30">
              <rect width="30" height="30" fill={hexValue} />
            </svg>
          </label>
        ))}
      </div>
    </div>
  );
};
