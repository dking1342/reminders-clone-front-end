import '../styles/listItemTag.css';
import { Tag } from '../../../types/tags';

type ListItemTagProps = {
  tag: Tag;
  isDone: boolean;
};

export const ListItemTag = ({ tag, isDone }: ListItemTagProps) => {
  return (
    <span
      role={'contentinfo'}
      aria-label="listItemTag"
      data-testid="listItemTag"
      key={tag._id}
      className={`subtitle-2 ${isDone ? 'isDone' : null}`}
    >
      {tag.name}
    </span>
  );
};
