type InfoIconProps = {};

export const InfoIcon = ({}: InfoIconProps) => {
  return (
    <svg
      viewBox="0 0 50 50"
      xmlns="http://www.w3.org/2000/svg"
      width={25}
      height={25}
    >
      <g stroke={'#30B4FF'} fill="transparent">
        <circle cx="25" cy="25" r="23" strokeWidth={3} />
        <circle cx="25" cy="12" r="3" fill={'#30B4FF'} />
        <line
          x1="25"
          y1="23"
          x2="25"
          y2="38"
          strokeWidth={4}
          strokeLinecap="round"
        />
      </g>
    </svg>
  );
};
