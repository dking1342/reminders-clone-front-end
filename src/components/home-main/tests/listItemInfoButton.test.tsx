import { screen, render, fireEvent } from '@testing-library/react';
import { ListItemInfoButton } from '../components/ListItemInfoButton';

let testProps = {
  handleEditToggle: () => {},
};
const setup = () => {
  render(<ListItemInfoButton handleEditToggle={testProps.handleEditToggle} />);
};

describe('ListItemInfoButton Component Testing', () => {
  test('Click command is executed for the handleEditToggle function', () => {
    const handleClick = jest.spyOn(testProps, 'handleEditToggle');
    setup();
    const btn = screen.getByTestId('listItemInfoButton');
    fireEvent.click(btn);
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
