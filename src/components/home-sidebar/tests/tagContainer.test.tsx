import '@testing-library/jest-dom/extend-expect';
import { screen, render } from '@testing-library/react';
import { ReactElement, JSXElementConstructor } from 'react';
import { TagsContext } from '../../../store/TagsContext';
import { TagsContainer } from '../components/TagContainer';

const setup = (
  ui: ReactElement<any, string | JSXElementConstructor<any>>,
  { providerProps, ...renderOptions }: any
) => {
  return render(
    <TagsContext.Provider value={providerProps}>{ui}</TagsContext.Provider>,
    renderOptions
  );
};

describe('TagContainer Component Tests', () => {
  test('Error message showing when no tags are in array', async () => {
    const text = 'No Tags Available';
    const providerProps = {
      ...TagsContext,
      tags: [],
    };
    setup(<TagsContainer />, { providerProps });
    const error = await screen.findByText(text);
    expect(error).toBeInTheDocument();
    expect(error.textContent).toBe(text);
  });
});
