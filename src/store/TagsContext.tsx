import { createContext, useContext, useEffect, useReducer } from 'react';
import { useQuery } from 'react-query';
import { tagApiGetAllUrl } from '../api/tags/constants/urls';
import { fetchTags } from '../api/tags/fetchRequest';
import { Tag } from '../types/tags';
import { Todo } from '../types/todos';
import { tagListReducer } from './tags/tagListReducer';
import { TAGS_CONSTANTS } from './tags/tagsConstants';
import { tagsReducer } from './tags/tagsReducer';
import { TodosContext } from './TodosContext';

export const tagsInitialState: Tag[] = [];

export const TagsContext = createContext({
  tags: tagsInitialState,
  tagList: tagsInitialState,
  handleGetTags: (todos: Todo[]) => {},
  handleCreateTag: (tag: Tag) => {},
  handleToggleTag: (id: string | number | undefined) => {},
  handleGetAllTags: (tags: Tag[]) => {},
  handleAddTagToAllTags: (tag: Tag) => {},
});

type TagsProviderProps = {
  children: any;
};

export const TagsProvider = ({ children }: TagsProviderProps) => {
  const [state, dispatch] = useReducer(tagsReducer, tagsInitialState);
  const [tagListState, tagListDispatch] = useReducer(
    tagListReducer,
    tagsInitialState
  );
  const { todos } = useContext(TodosContext);

  // actions
  // tags state
  const handleGetTags = (todos: Todo[]) => {
    dispatch({
      type: TAGS_CONSTANTS.GET_TAGS,
      payload: todos,
    });
  };
  const handleCreateTag = (tag: Tag) => {
    dispatch({
      type: TAGS_CONSTANTS.CREATE_TAG,
      payload: tag,
    });
  };
  const handleToggleTag = (id: string | number | undefined) => {
    dispatch({
      type: TAGS_CONSTANTS.TOGGLE_TAG,
      payload: id,
    });
  };

  // tagList state
  const handleGetAllTags = (tags: Tag[]) => {
    tagListDispatch({
      type: TAGS_CONSTANTS.GET_ALL_TAGS,
      payload: tags,
    });
  };
  const handleAddTagToAllTags = (tag: Tag) => {
    tagListDispatch({
      type: TAGS_CONSTANTS.ADD_ALL_TAGS,
      payload: tag,
    });
  };

  // queries
  const { isSuccess, data } = useQuery('tags', async () => {
    const url = tagApiGetAllUrl;
    return await fetchTags(url);
  });

  useEffect(() => {
    handleGetTags(todos);
  }, [todos]);

  useEffect(() => {
    if (isSuccess) {
      const tags: Tag[] = data.data as Tag[];
      handleGetAllTags(tags);
    }
  }, [isSuccess, data]);

  const value = {
    tags: state,
    tagList: tagListState,
    handleGetTags,
    handleGetAllTags,
    handleCreateTag,
    handleToggleTag,
    handleAddTagToAllTags,
  };

  return <TagsContext.Provider value={value}>{children}</TagsContext.Provider>;
};
