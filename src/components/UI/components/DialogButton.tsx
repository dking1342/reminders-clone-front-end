import '../styles/dialogButton.css';
import { ReactNode, useContext } from 'react';
import { ListsContext } from '../../../store/ListsContext';
import { TodosContext } from '../../../store/TodosContext';
import { MenusContext } from '../../../store/MenusContext';
import { MenuState } from '../../../types/menus';

type DialogButtonProps = {
  selector: '#listMenuDialog' | '#todoMenuDialog';
  children: ReactNode;
  action: MenuState;
  id?: string | number | undefined;
};

export const DialogButton = ({
  id,
  selector,
  children,
  action,
}: DialogButtonProps) => {
  const { handleSetListId } = useContext(ListsContext);
  // const { handleSetTodoId } = useContext(TodosContext);
  const {
    handleMenuListSetIsOpen,
    handleMenuTodoSetIsOpen,
    handleMenuListSetState,
    handleMenuTodoSetState,
    handleSetTodoMenuIdFormValues,
  } = useContext(MenusContext);

  const handleDialogOpen = () => {
    const dialog = document.querySelector(`${selector}`) as HTMLDialogElement;
    dialog.showModal();

    // set id
    if (selector === '#listMenuDialog' && id) {
      handleSetListId(id);
    }
    if (selector === '#todoMenuDialog' && id) {
      handleSetTodoMenuIdFormValues(id);
    }

    // toggle menu open
    if (selector === '#listMenuDialog') {
      handleMenuListSetState(action);
      handleMenuListSetIsOpen(true);
    } else {
      handleMenuTodoSetState(action);
      handleMenuTodoSetIsOpen(true);
    }
  };

  return (
    <button
      onClick={handleDialogOpen}
      className="dialog-button"
      role={'button'}
      data-testid="dialogButton"
    >
      {children}
    </button>
  );
};
