import HttpStatusCode from '../../enums/httpStatusCode';
import { ApiAction, Res } from '../../types/api';
import { MenuTodo } from '../../types/menus';
import { Todo } from '../../types/todos';

let todoResponse: Res<Todo> = {
  timestamp: new Date(Date.now()),
  status: HttpStatusCode.NOT_FOUND,
};

let todoMenuResponse: Res<MenuTodo> = {
  timestamp: new Date(Date.now()),
  status: HttpStatusCode.NOT_FOUND,
};

export const fetchTodos = async (
  url: string,
  method?: ApiAction,
  body?: Todo
) => {
  try {
    let response: Response;

    if ((method && body) || method === 'DELETE') {
      response = await fetch(url, {
        method,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(body),
      });
    } else {
      response = await fetch(url);
    }

    if (response.ok) {
      const data: Res<Todo> = await response.json();
      return data;
    } else {
      todoResponse = {
        ...todoResponse,
        error: 'Network error',
      };
      return todoResponse;
    }
  } catch (error) {
    const e = error as Error;
    todoResponse = {
      ...todoResponse,
      error: e.message,
    };
    return todoResponse;
  }
};
export const fetchMenuTodos = async (
  url: string,
  method?: ApiAction,
  body?: MenuTodo
) => {
  try {
    let response: Response;

    if ((method && body) || method === 'DELETE') {
      response = await fetch(url, {
        method,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(body),
      });
    } else {
      response = await fetch(url);
    }

    if (response.ok) {
      const data: Res<Todo> = await response.json();
      return data;
    } else {
      todoMenuResponse = {
        ...todoMenuResponse,
        error: 'Network error',
      };
      return todoMenuResponse;
    }
  } catch (error) {
    const e = error as Error;
    todoMenuResponse = {
      ...todoMenuResponse,
      error: e.message,
    };
    return todoMenuResponse;
  }
};
