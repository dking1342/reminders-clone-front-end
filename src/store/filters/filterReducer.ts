import { Filter } from '../../types/filters';
import { Tag } from '../../types/tags';
import { FILTERS_CONSTANTS } from './filterConstants';

export const filterReducer = (
  state: Filter,
  action: { type: string; payload: any }
) => {
  const { type, payload } = action;

  switch (type) {
    case FILTERS_CONSTANTS.FILTER_POPULATE_ALL:
      state = payload;
      return state;

    case FILTERS_CONSTANTS.FILTER_SET_FILTERS_SEARCH:
      let search: string = payload;
      state = { ...state, search };
      return state;

    case FILTERS_CONSTANTS.FILTER_SET_FILTERS_LIST:
      let list: string = payload;
      state = { ...state, list };
      return state;

    case FILTERS_CONSTANTS.FILTER_SET_FILTERS_SHOWING:
      state = { ...state, isShowing: !state.isShowing };
      return state;

    case FILTERS_CONSTANTS.FILTER_SET_FILTERS_TAGS:
      const { id, tags } = payload as { id: string | number; tags: Tag[] };
      const newTags = tags.map((tag) => {
        if (tag._id === id) {
          tag = {
            ...tag,
            isSelected: !tag.isSelected,
          };
        }
        return tag;
      });

      const filteredTags = newTags.filter((tag) => tag.isSelected);
      state = { ...state, tags: filteredTags };
      return state;

    default:
      return state;
  }
};
