import { List } from '../../types/lists';
import { LISTS_CONSTANTS } from './listsConstants';

export const listsReducer = (
  state: List[],
  action: { type: string; payload: any }
) => {
  const { type, payload } = action;

  switch (type) {
    case LISTS_CONSTANTS.LIST_POPULATE:
      state = payload;
      return state;

    case LISTS_CONSTANTS.LIST_ADD_ITEM:
      const listAdd: List = payload;
      return [...state, listAdd];

    case LISTS_CONSTANTS.LIST_UPDATE_ITEM:
      const lists = state;
      let listUpdate: List = payload;

      const updatedList = lists.map((l) => {
        if (l._id === listUpdate._id) {
          listUpdate = {
            ...listUpdate,
            updatedAt: Date.now(),
          };
          return listUpdate;
        }
        return l;
      });
      state = updatedList;
      return state;

    case LISTS_CONSTANTS.LIST_DELETE_ITEM:
      let id: number = payload;
      let filteredList: List[] = [];
      filteredList = state.filter((list) => list._id !== id);
      state = filteredList;
      return state;

    default:
      return state;
  }
};
