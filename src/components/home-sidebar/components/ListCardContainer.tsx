import { useContext } from 'react';
import { TodosContext } from '../../../store/TodosContext';
import { ListCards } from '../../UI/components/Cards';
import '../styles/listCardContainer.css';

type ListCardContainerProps = {};

const ListCardContainer = ({}: ListCardContainerProps) => {
  const { todos } = useContext(TodosContext);

  return (
    <div className="card-container">
      <ListCards name="All" quantity={todos.length} icon="list" />
      <ListCards
        name="Flagged"
        quantity={todos.filter((t) => t.isFlagged).length}
        icon="flag"
      />
    </div>
  );
};

export default ListCardContainer;
