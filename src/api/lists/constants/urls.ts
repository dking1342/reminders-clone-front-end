const baseUrl = 'http://157.230.217.83:80/api/lists';

export const listApiGetAllUrl = `${baseUrl}/get/all`;
export const listApiPostUrl = `${baseUrl}/create`;

export const listApiGetOneUrl = (id: string | number | undefined) => {
  return `${baseUrl}/get/one/${id}`;
};

export const listApiPutUrl = (id: string | number | undefined) => {
  return `${baseUrl}/update/${id}`;
};
export const listApiDeleteUrl = (id: string | number | undefined) => {
  return `${baseUrl}/delete/${id}`;
};
