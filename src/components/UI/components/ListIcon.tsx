import { useState, useEffect } from 'react';
import { colorPicker } from '../../../utils/colorPicker';

type ListIconProps = {
  color: string;
  // color: '200' | '300' | '400' | '500' | '600' | '700' | '800';
};

export const ListIcon = ({ color }: ListIconProps) => {
  const dimension = '25';
  const circleStartingPoint = '16';
  const circleRadius = '2';
  const lineOneY = '17';
  const lineTwoY = '25';
  const lineThreeY = '33';

  const lineStartingPoint = '23';
  const lineEndPoint = '35';
  const [fill, setFill] = useState('#F4CA46');

  useEffect(() => {
    const hexValue = colorPicker(color);
    setFill(hexValue);
  }, [color]);

  return (
    <svg
      viewBox="0 0 50 50"
      xmlns="http://www.w3.org/2000/svg"
      width={dimension}
      height={dimension}
      fill={fill}
      data-testid="listIcon"
    >
      <circle cx={dimension} cy={dimension} r={dimension} />
      <g fill={'#fff'} stroke={'#fff'}>
        <circle cx={circleStartingPoint} cy={lineOneY} r={circleRadius} />
        <circle cx={circleStartingPoint} cy={lineTwoY} r={circleRadius} />
        <circle cx={circleStartingPoint} cy={lineThreeY} r={circleRadius} />
        <line
          x1={lineStartingPoint}
          y1={lineOneY}
          x2={lineEndPoint}
          y2={lineOneY}
          strokeWidth={3}
          strokeLinecap="round"
        />
        <line
          x1={lineStartingPoint}
          y1={lineTwoY}
          x2={lineEndPoint}
          y2={lineTwoY}
          strokeWidth={3}
          strokeLinecap="round"
        />
        <line
          x1={lineStartingPoint}
          y1={lineThreeY}
          x2={lineEndPoint}
          y2={lineThreeY}
          strokeWidth={3}
          strokeLinecap="round"
        />
      </g>
    </svg>
  );
};
