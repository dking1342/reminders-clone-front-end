import { List } from '../types/lists';
import { Todo } from '../types/todos';

export const initList: List = {
  _id: 0,
  name: '',
  color: '200',
  createdAt: Date.now(),
  updatedAt: Date.now(),
};
export const menuInitState: Todo = {
  _id: 0,
  name: '',
  tags: [],
  isFlagged: false,
  isDone: false,
  list: initList,
};
