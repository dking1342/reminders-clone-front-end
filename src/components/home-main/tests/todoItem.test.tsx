import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, screen } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { todosDataTest1 } from '../../../data/todos';
import { Todo } from '../../../types/todos';
import { TodoItem } from '../components/TodoItem';

type TodoItemProps = {
  todo: Todo;
};
let props: TodoItemProps = {
  todo: {} as Todo,
};

const setup = () =>
  render(
    <QueryClientProvider client={new QueryClient()}>
      <TodoItem todo={props.todo} />
    </QueryClientProvider>
  );

describe('TodoItem Component Tests', () => {
  test('When isEditing is true then the container class includes todos-item-row-isEditing', () => {
    const todo = todosDataTest1[1];
    props = { ...props, todo };
    const className = 'todos-item-row-isEditing';

    setup();

    const btn = screen.getByTestId('listItemInfoButton');
    fireEvent.click(btn);
    const section = screen
      .getByTestId('todoItem-container')
      .getAttribute('class')
      ?.split(' ')
      .includes(className);
    expect(section).toBeTruthy();
  });
});
