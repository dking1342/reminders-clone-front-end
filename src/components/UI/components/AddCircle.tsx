type AddCircleProps = {};

export const AddCircle = ({}: AddCircleProps) => {
  return (
    <svg
      viewBox="0 0 50 50"
      xmlns="http://www.w3.org/2000/svg"
      width={25}
      height={25}
    >
      <g stroke="white" strokeWidth={3} strokeLinecap="round">
        <circle cx={25} cy={25} r={20} />
        <line x1="25" y1="15" x2="25" y2="35" />
        <line x1="15" y1="25" x2="35" y2="25" />
      </g>
    </svg>
  );
};
