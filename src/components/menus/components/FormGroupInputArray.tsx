import { useContext } from 'react';
import { MenusContext } from '../../../store/MenusContext';
import { TagsContext } from '../../../store/TagsContext';
import { AddCircle } from '../../UI/components/AddCircle';
import '../styles/forms.css';

type FormGroupInputArrayProps = {
  name: string;
  title: string;
  value: string;
};

export const FormGroupInputArray = ({
  name,
  title,
  value,
}: FormGroupInputArrayProps) => {
  const { handleMenuTagSearchInput, handleMenuTagAdd } =
    useContext(MenusContext);
  const { tagList } = useContext(TagsContext);

  return (
    <div className="form-group caption-2">
      <label htmlFor={name}>{title}</label>
      <input
        type="search"
        name={name}
        className="caption-2 form-input"
        onChange={(e) => handleMenuTagSearchInput(e.target.value, tagList)}
        value={value}
      />
      <button
        value="tags"
        onClick={() => handleMenuTagAdd(value, tagList)}
        style={{
          backgroundColor: 'transparent',
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <AddCircle />
      </button>
    </div>
  );
};
