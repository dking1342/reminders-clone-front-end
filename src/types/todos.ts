import { List } from './lists';
import { Tag } from './tags';

export type Todo = {
  _id?: string | number;
  name: string;
  tags: Tag[];
  isFlagged: boolean;
  isDone: boolean;
  list: List;
  // createdAt:number;
  // updatedAt:number;
};
