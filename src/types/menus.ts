import { Tag } from './tags';

export type MenuState = 'POST' | 'PUT';
export type MenuAction = 'delete' | 'add';
export type MenuIsOpen = boolean;
export type MenuType = 'todo' | 'list';
export type MenuOptions = { state: MenuState; isOpen: MenuIsOpen };
export type MenuList = {
  list: MenuOptions;
  todo: MenuOptions;
  type: MenuType;
  tagInput: string;
  filteredTags: Tag[];
  selectedTags: Tag[];
  errors: string;
  submitted: boolean;
};
export type MenuTodo = {
  _id?: string | number;
  name: string;
  tags: any;
  isFlagged: boolean;
  isDone: boolean;
  list: string | number | undefined;
};
