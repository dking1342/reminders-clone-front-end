import { useContext } from 'react';
import { FiltersContext } from '../../../store/FiltersContext';
import { ListsContext } from '../../../store/ListsContext';
import { MobileContext } from '../../../store/MobileContext';
import { List } from '../../../types/lists';
import { ListIcon } from '../../UI/components/ListIcon';
import '../styles/listRowBody.css';

type ListRowBodyProps = {
  list: List;
  quantity: number;
};

export const ListRowBody = ({ list, quantity }: ListRowBodyProps) => {
  const { handleMobileToggle } = useContext(MobileContext);
  const { handleFiltersSetList } = useContext(FiltersContext);
  const { handleSetListName, handleSetListId } = useContext(ListsContext);
  return (
    <button
      className="sidebar-list-body"
      onClick={() => {
        handleSetListName(list.name);
        handleSetListId(list._id);
        handleFiltersSetList(list.name);
        handleMobileToggle();
      }}
    >
      <ListIcon color={list.color} />
      <span
        data-testid="listRowBodyName"
        className="body-1 sidebar-list-body-name"
        role={'contentinfo'}
      >
        {list.name}
      </span>
      <span
        data-testid="listRowBodyQuantity"
        className="subtitle-2 sidebar-list-body-quantity"
      >
        {quantity}
      </span>
    </button>
  );
};
