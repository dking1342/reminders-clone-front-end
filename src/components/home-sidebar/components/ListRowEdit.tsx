import '../styles/listRowEdit.css';
import { ListRowEditButton } from './ListRowEditButton';
import { ListRowEditButtons } from './ListRowEditButtons';

type ListRowEditProps = {
  id: string | number | undefined;
  handleEditOpenToggle: () => void;
};

export const ListRowEdit = ({ id, handleEditOpenToggle }: ListRowEditProps) => {
  return (
    <div className="list-row-edit-container">
      <ListRowEditButton id={id} handleEditOpenToggle={handleEditOpenToggle} />
      <ListRowEditButtons id={id} handleEditOpenToggle={handleEditOpenToggle} />
    </div>
  );
};
