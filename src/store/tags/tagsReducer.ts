import { Tag } from '../../types/tags';
import { Todo } from '../../types/todos';
import { TAGS_CONSTANTS } from './tagsConstants';

export const tagsReducer = (
  state: Tag[],
  action: { type: string; payload: any }
) => {
  const { type, payload } = action;

  switch (type) {
    case TAGS_CONSTANTS.GET_TAGS:
      state = [];
      const todos: Todo[] = payload;
      let tagsArray: Tag[] = [];
      todos.forEach((todo) => {
        if (todo.tags.length) {
          todo.tags.forEach((tag) => {
            let isMatch = tagsArray.find((item) => item.name === tag.name);
            !isMatch ? tagsArray.push(tag) : null;
          });
        }
      });
      tagsArray.forEach((tag) => state.push(tag));
      return state;

    case TAGS_CONSTANTS.CREATE_TAG:
      const newTag: Tag = payload as Tag;
      state = { ...state, ...newTag };
      return state;

    case TAGS_CONSTANTS.TOGGLE_TAG:
      state = state.map((tag) => {
        if (tag._id === payload) {
          tag = {
            ...tag,
            isSelected: !tag.isSelected,
          };
        }
        return tag;
      });
      return state;

    default:
      return state;
  }
};
