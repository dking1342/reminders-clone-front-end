import { Todo } from '../types/todos';

export const todosData: Todo[] = [
  {
    _id: 1123,
    name: 'Buy milk',
    tags: [
      {
        _id: 1,
        name: '#jobs',
        isSelected: false,
      },
      {
        _id: 2,
        name: '#career',
        isSelected: false,
      },
    ],
    isFlagged: false,
    isDone: false,
    list: {
      _id: 3,
      name: 'Chores',
      color: '200',
      createdAt: Date.now(),
      updatedAt: Date.now(),
    },
  },
  {
    _id: 2321,
    name: 'Make bed',
    tags: [],
    isFlagged: false,
    isDone: false,
    list: {
      _id: 4,
      name: 'Homework',
      color: '500',
      createdAt: Date.now() + 10000,
      updatedAt: Date.now() + 10000,
    },
  },
  {
    _id: 32312,
    name: 'Rent movie',
    tags: [
      {
        _id: 3,
        name: '#fun',
        isSelected: false,
      },
      {
        _id: 2,
        name: '#career',
        isSelected: false,
      },
    ],
    isFlagged: false,
    isDone: false,
    list: {
      _id: 3,
      name: 'Chores',
      color: '200',
      createdAt: Date.now(),
      updatedAt: Date.now(),
    },
  },
];

export const todosDataTest1: Todo[] = [
  {
    _id: 1123,
    name: 'Buy milk',
    tags: [
      {
        _id: 1,
        name: '#jobs',
        isSelected: false,
      },
      {
        _id: 2,
        name: '#career',
        isSelected: false,
      },
    ],
    isFlagged: false,
    isDone: true,
    list: {
      _id: 3,
      name: 'Chores',
      color: '200',
      createdAt: Date.now(),
      updatedAt: Date.now(),
    },
  },
  {
    _id: 2321,
    name: 'Make bed',
    tags: [],
    isFlagged: false,
    isDone: false,
    list: {
      _id: 4,
      name: 'Homework',
      color: '500',
      createdAt: Date.now() + 10000,
      updatedAt: Date.now() + 10000,
    },
  },
  {
    _id: 32312,
    name: 'Rent movie',
    tags: [
      {
        _id: 3,
        name: '#fun',
        isSelected: false,
      },
      {
        _id: 2,
        name: '#career',
        isSelected: false,
      },
    ],
    isFlagged: true,
    isDone: true,
    list: {
      _id: 3,
      name: 'Chores',
      color: '200',
      createdAt: Date.now(),
      updatedAt: Date.now(),
    },
  },
];
