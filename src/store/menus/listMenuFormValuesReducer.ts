import { List } from '../../types/lists';
import { ActionType } from '../../types/reducers';
import { listMenuFormValuesInitialState } from '../MenusContext';
import { MENU_CONSTANTS } from './menusConstants';

export const listMenuFormValuesReducer = (state: List, action: ActionType) => {
  const { type, payload } = action;

  switch (type) {
    case MENU_CONSTANTS.MENU_LIST_GET_FORM_VALUES:
      let { id, list } = payload as {
        id: number;
        list: List[];
      };
      const getList = list.find((l) => l._id === id);
      return getList ? (state = getList) : state;

    case MENU_CONSTANTS.MENU_LIST_SET_FORM_VALUES:
      const { input, key } = payload as { input: string; key: string };
      state = { ...state, [key]: input };
      return state;

    case MENU_CONSTANTS.MENU_LIST_RESET_FORM_VALUES:
      state = listMenuFormValuesInitialState;
      return state;

    default:
      return state;
  }
};
