import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { JSXElementConstructor, ReactElement } from 'react';
import {
  filterInitialState,
  FiltersContext,
} from '../../../store/FiltersContext';
import SearchTodos from '../components/SearchTodos';

const setup = (
  ui: ReactElement<any, string | JSXElementConstructor<any>>,
  { providerProps, ...renderOptions }: any
) => {
  return render(
    <FiltersContext.Provider value={providerProps}>
      {ui}
    </FiltersContext.Provider>,
    renderOptions
  );
};

describe('SearchTodos Component Tests', () => {
  test('Search input is empty', () => {
    const filter = filterInitialState;
    const providerProps = { ...FiltersContext, filter };
    setup(<SearchTodos />, { providerProps });
    const input = screen.getByTestId('search') as HTMLInputElement;
    expect(input).toBeInTheDocument();
    expect(input).toHaveValue('');
  });
  test('OnChange function is called when typing into input element', async () => {
    const text = '';
    const filter = { ...filterInitialState, search: text };
    const providerProps = {
      ...FiltersContext,
      filter,
      handleFiltersSetSearch: (search: string) => {},
    };
    const spy = jest.spyOn(providerProps, 'handleFiltersSetSearch');
    setup(<SearchTodos />, { providerProps });

    const element = screen.getByTestId('search') as HTMLInputElement;
    await userEvent.type(element, 'a');
    expect(spy).toHaveBeenCalledTimes(1);
  });
  test('Search element has value of xxx when filter has search value of xxx', async () => {
    const text = 'xxx';
    const filter = { ...filterInitialState, search: text };
    const providerProps = {
      ...FiltersContext,
      filter,
    };
    setup(<SearchTodos />, { providerProps });

    const element = screen.getByTestId('search') as HTMLInputElement;
    expect(element).toBeInTheDocument();
    expect(element).toHaveValue(text);
  });
});
