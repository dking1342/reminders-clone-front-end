import { createContext, useReducer } from 'react';
import { List } from '../types/lists';
import {
  MenuList,
  MenuOptions,
  MenuState,
  MenuTodo,
  MenuType,
} from '../types/menus';
import { Tag } from '../types/tags';
import { Todo } from '../types/todos';
import { listMenuFormValuesReducer } from './menus/listMenuFormValuesReducer';
import { MENU_CONSTANTS } from './menus/menusConstants';
import { menusReducer } from './menus/menusReducer';
import { todoMenuFormValuesReducer } from './menus/todoMenuFormValuesReducer';

export const menusInitialState: MenuList = {
  list: { state: 'POST', isOpen: false },
  todo: { state: 'POST', isOpen: false },
  type: 'list',
  tagInput: '',
  filteredTags: [],
  selectedTags: [],
  errors: '',
  submitted: false,
};
export const listMenuFormValuesInitialState: List = {
  name: '',
  color: '200',
  createdAt: Date.now(),
  updatedAt: Date.now(),
};
export const todoMenuFormValuesIntialState: MenuTodo = {
  name: '',
  tags: [],
  isFlagged: false,
  isDone: false,
  list: '',
};

export const MenusContext = createContext({
  menus: menusInitialState,
  listMenuFormValues: listMenuFormValuesInitialState,
  todoMenuFormValues: todoMenuFormValuesIntialState,
  handleMenuListSetAll: (input: MenuOptions) => {},
  handleMenuListSetState: (state: MenuState) => {},
  handleMenuListSetIsOpen: (isOpen: boolean) => {},
  handleMenuTodoSetAll: (input: MenuOptions) => {},
  handleMenuTodoSetState: (state: MenuState) => {},
  handleMenuTodoSetIsOpen: (isOpen: boolean) => {},
  handleMenuSetType: (type: MenuType) => {},
  handleMenuTagClick: (id: string | number | undefined, allTags: Tag[]) => {},
  handleMenuSetTags: (id: string | number, todo: Todo[]) => {},
  handleMenuTagAdd: (input: string, tags: Tag[]) => {},
  handleMenuTagUpdate: (input: Tag) => {},
  handleMenuTagDelete: (id: string | number | undefined) => {},
  handleMenuTagSearchInput: (e: string, tags: Tag[]) => {},
  handleMenuReset: () => {},
  handleGetListMenuFormValues: (id: string | number, list: List[]) => {},
  handleSetListMenuFormValues: (input: string, key: string) => {},
  handleResetListMenuFormValues: () => {},
  handleGetTodoMenuFormValues: (id: string | number, todos: Todo[]) => {},
  handleSetTodoMenuFormValues: (
    input: string,
    key: string,
    inputType: string
  ) => {},
  handleResetTodoMenuFormValues: () => {},
  handleSetTodoMenuListFormValues: (list: string | number) => {},
  handleSetTodoMenuTagsFormValues: (tags: Tag[]) => {},
  handleSetTodoMenuIdFormValues: (id?: string | number | undefined) => {},
  handleMenuSubmitted: (input: boolean) => {},
  handleMenuError: (error: string) => {},
});

type MenusProviderProps = {
  children: any;
};

export const MenusProvider = ({ children }: MenusProviderProps) => {
  const [state, dispatch] = useReducer(menusReducer, menusInitialState);
  const [listMenuFormValuesState, listMenuFormValuesDispatch] = useReducer(
    listMenuFormValuesReducer,
    listMenuFormValuesInitialState
  );
  const [todoMenuFormValuesState, todoMenuFormValuesDispatch] = useReducer(
    todoMenuFormValuesReducer,
    todoMenuFormValuesIntialState
  );

  // actions \\
  //// LIST MENU \\\\
  const handleMenuListSetAll = (input: MenuOptions) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_LIST_SET_ALL,
      payload: input,
    });
  };
  const handleMenuListSetState = (state: MenuState) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_LIST_SET_STATE,
      payload: state,
    });
  };
  const handleMenuListSetIsOpen = (isOpen: boolean) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_LIST_SET_IS_OPEN,
      payload: isOpen,
    });
  };

  // list menu form
  const handleGetListMenuFormValues = (id: string | number, list: List[]) => {
    listMenuFormValuesDispatch({
      type: MENU_CONSTANTS.MENU_LIST_GET_FORM_VALUES,
      payload: { id, list },
    });
  };
  const handleSetListMenuFormValues = (input: string, key: string) => {
    listMenuFormValuesDispatch({
      type: MENU_CONSTANTS.MENU_LIST_SET_FORM_VALUES,
      payload: { input, key },
    });
  };
  const handleResetListMenuFormValues = () => {
    listMenuFormValuesDispatch({
      type: MENU_CONSTANTS.MENU_LIST_RESET_FORM_VALUES,
      payload: null,
    });
  };

  //// TODO MENU \\\\
  const handleMenuTodoSetAll = (input: MenuOptions) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_ALL,
      payload: input,
    });
  };
  const handleMenuTodoSetState = (state: MenuState) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_STATE,
      payload: state,
    });
  };
  const handleMenuTodoSetIsOpen = (isOpen: boolean) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_IS_OPEN,
      payload: isOpen,
    });
  };

  // todo menu form
  const handleGetTodoMenuFormValues = (id: string | number, todos: Todo[]) => {
    todoMenuFormValuesDispatch({
      type: MENU_CONSTANTS.MENU_TODO_GET_FORM_VALUES,
      payload: { id, todos },
    });
  };
  const handleSetTodoMenuFormValues = (
    input: string,
    key: string,
    inputType: string
  ) => {
    todoMenuFormValuesDispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_FORM_VALUES,
      payload: { input, key, inputType },
    });
  };
  const handleResetTodoMenuFormValues = () => {
    todoMenuFormValuesDispatch({
      type: MENU_CONSTANTS.MENU_TODO_RESET_FORM_VALUES,
      payload: null,
    });
  };
  const handleSetTodoMenuTagsFormValues = (tags: Tag[]) => {
    const tagStrings = tags.map((t) => t._id);
    todoMenuFormValuesDispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_TAGS_FORM_VALUES,
      payload: tagStrings,
    });
  };
  const handleSetTodoMenuIdFormValues = (id?: string | number | undefined) => {
    todoMenuFormValuesDispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_ID_FORM_VALUES,
      payload: id,
    });
  };
  const handleSetTodoMenuListFormValues = (list: string | number) => {
    todoMenuFormValuesDispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_LIST_FORM_VALUES,
      payload: list,
    });
  };

  // todo menu tags
  const handleMenuSetTags = (id: string | number, todos: Todo[]) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_SELECTED_TAGS,
      payload: { id, todos },
    });
  };
  const handleMenuTagSearchInput = (e: string, tags: Tag[]) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_TAG_INPUT,
      payload: { e, tags },
    });
  };
  const handleMenuTagClick = (
    id: string | number | undefined,
    allTags: Tag[]
  ) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_SELECTED_TAGS_CLICK,
      payload: { id, allTags },
    });
  };
  const handleMenuTagAdd = (input: string, tags: Tag[]) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_SELECTED_TAGS_ADD,
      payload: { input, tags },
    });
  };
  const handleMenuTagUpdate = (input: Tag) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_SELECTED_TAGS_UPDATE,
      payload: input,
    });
  };
  const handleMenuTagDelete = (id: string | number | undefined) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_TODO_SET_SELECTED_TAGS_DELETE,
      payload: id,
    });
  };

  // GENERAL ACTIONS \\
  const handleMenuSetType = (type: MenuType) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_TYPE_SET,
      payload: type,
    });
  };
  const handleMenuReset = () => {
    dispatch({
      type: MENU_CONSTANTS.MENU_STATE_RESET,
      payload: null,
    });
  };
  const handleMenuSubmitted = (input: boolean) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_STATE_SUBMITTED,
      payload: input,
    });
  };
  const handleMenuError = (error: string) => {
    dispatch({
      type: MENU_CONSTANTS.MENU_STATE_ERROR,
      payload: error,
    });
  };

  const value = {
    menus: state,
    listMenuFormValues: listMenuFormValuesState,
    todoMenuFormValues: todoMenuFormValuesState,
    handleMenuListSetAll,
    handleMenuListSetState,
    handleMenuListSetIsOpen,
    handleMenuTodoSetAll,
    handleMenuTodoSetState,
    handleMenuTodoSetIsOpen,
    handleMenuSetType,
    handleMenuTagSearchInput,
    handleMenuTagClick,
    handleMenuSetTags,
    handleMenuTagAdd,
    handleMenuTagDelete,
    handleMenuReset,
    handleGetListMenuFormValues,
    handleSetListMenuFormValues,
    handleResetListMenuFormValues,
    handleGetTodoMenuFormValues,
    handleSetTodoMenuFormValues,
    handleResetTodoMenuFormValues,
    handleSetTodoMenuTagsFormValues,
    handleSetTodoMenuIdFormValues,
    handleMenuSubmitted,
    handleMenuError,
    handleSetTodoMenuListFormValues,
    handleMenuTagUpdate,
  };

  return (
    <MenusContext.Provider value={value}>{children}</MenusContext.Provider>
  );
};
