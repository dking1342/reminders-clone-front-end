import { useContext } from 'react';
import { MenusContext } from '../../../store/MenusContext';
import { Tag } from '../../../types/tags';
import { ListTags } from '../../home-sidebar/components/ListTags';
import '../styles/forms.css';

type MenuTagOutputProps = {};

export const MenuTagOutput = ({}: MenuTagOutputProps) => {
  const { menus } = useContext(MenusContext);

  return (
    <div>
      {menus.selectedTags.length ? (
        <>
          <p className="caption-2">Tag List:</p>
          <p style={{ fontSize: 12, color: '#ff0000' }}>
            Click on tag to remove
          </p>
          <ListTags tags={menus.selectedTags} action={'delete'} />
        </>
      ) : (
        <></>
      )}
    </div>
  );
};
