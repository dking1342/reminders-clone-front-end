import '../styles/todoItemTags.css';
import { Tag } from '../../../types/tags';
import { ListItemTag } from './ListItemTag';

type TodoItemTagsProps = {
  tags: Tag[];
  isDone: boolean;
};

export const TodoItemTags = ({ tags, isDone }: TodoItemTagsProps) => {
  return (
    <div className="todos-item-tags">
      {tags.map((tag) => (
        <ListItemTag tag={tag} key={tag._id} isDone={isDone} />
      ))}
    </div>
  );
};
