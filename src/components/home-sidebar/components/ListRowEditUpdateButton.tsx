import { DialogButton } from '../../UI/components/DialogButton';
import { EditIcons } from '../../UI/components/EditIcons';

type ListRowEditUpdateButtonProps = {
  id: string | number | undefined;
  handleEditOpenToggle: () => void;
};

export const ListRowEditUpdateButton = ({
  id,
  handleEditOpenToggle,
}: ListRowEditUpdateButtonProps) => {
  return (
    <div onClick={handleEditOpenToggle}>
      <DialogButton selector="#listMenuDialog" action="PUT" id={id}>
        <EditIcons size="large" />
      </DialogButton>
    </div>
  );
};
