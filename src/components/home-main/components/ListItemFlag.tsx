import { useContext } from 'react';
import { useMutation } from 'react-query';
import { todoApiPutUrl } from '../../../api/todos/constants/urls';
import { fetchTodos } from '../../../api/todos/fetchRequest';
import { TodosContext } from '../../../store/TodosContext';
import { Todo } from '../../../types/todos';
import { FlagIcon } from '../../UI/components/FlagIcon';
import '../styles/listItemFlag.css';

type ListItemFlagProps = {
  id: string | number | undefined;
  isFlaggedIcon: boolean;
};

export const ListItemFlag = ({ id, isFlaggedIcon }: ListItemFlagProps) => {
  const { todos, handleUpdateTodoFlag } = useContext(TodosContext);
  const todo = todos.find((t) => t._id === id) as Todo;

  const todoMutation = useMutation(
    (todoId: string | number) => {
      const url = todoApiPutUrl(todoId);
      return fetchTodos(url, 'PUT', { ...todo, isFlagged: !todo?.isFlagged });
    },
    {
      onSuccess: (data, variables) => {
        if (data.status === 201) {
          handleUpdateTodoFlag(variables);
        }
      },
    }
  );

  return (
    <div className="todos-item-flag" role={'contentinfo'}>
      <button
        role={'button'}
        data-testid="listItemFlag-button"
        onClick={() => todoMutation.mutate(id!)}
      >
        <FlagIcon isFlagged={isFlaggedIcon} />
      </button>
    </div>
  );
};
