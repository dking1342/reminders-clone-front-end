import { useContext } from 'react';
import { MobileContext } from '../../../store/MobileContext';
import '../styles/sidebar.css';
import ListCardContainer from './ListCardContainer';
import { ListsContainer } from './ListsContainer';
import SearchTodos from './SearchTodos';
import SidebarFooter from './SidebarFooter';
import { TagsContainer } from './TagContainer';

type SidebarProps = {};

const Sidebar = ({}: SidebarProps) => {
  const { isMobileList } = useContext(MobileContext);

  return (
    <section
      data-testid="sidebar"
      className={`sidebar-container ${
        !isMobileList && 'sidebar-container-closed'
      }`}
    >
      <SearchTodos />
      <ListCardContainer />
      <ListsContainer />
      <TagsContainer />
      <SidebarFooter />
    </section>
  );
};

export default Sidebar;
