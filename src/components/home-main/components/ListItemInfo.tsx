import '../styles/listItemInfo.css';
import { ListItemInfoButton } from './ListItemInfoButton';
import { ListItemInfoButtons } from './ListItemInfoButtons';

type ListItemInfoProps = {
  isEditing: boolean;
  handleEditToggle: () => void;
  id: string | number | undefined;
};

export const ListItemInfo = ({
  id,
  isEditing,
  handleEditToggle,
}: ListItemInfoProps) => {
  return (
    <div
      role={'contentinfo'}
      className={`todos-item-info ${
        isEditing ? 'todos-item-info-edit' : 'todos-item-info-noEdit'
      }`}
    >
      <ListItemInfoButton handleEditToggle={handleEditToggle} />
      <ListItemInfoButtons id={id} />
    </div>
  );
};
