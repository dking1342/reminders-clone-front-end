import { useContext, useEffect, useState } from 'react';
import { FiltersContext } from '../../../store/FiltersContext';
import { ListsContext } from '../../../store/ListsContext';
import { colorPicker } from '../../../utils/colorPicker';
import '../styles/mainHeaderTitle.css';

type MainHeaderTitleProps = {};

const MainHeaderTitle = ({}: MainHeaderTitleProps) => {
  const { filteredTodos } = useContext(FiltersContext);
  const { lists, listName } = useContext(ListsContext);
  const [color, setColor] = useState('#fff');

  useEffect(() => {
    if (lists.length) {
      let newColor: string = '';

      switch (listName.name) {
        case 'All':
          newColor = colorPicker('200');
          break;
        case 'Flagged':
          newColor = colorPicker('300');
          break;
        default:
          newColor = colorPicker(
            lists.find((list) => list.name === listName.name)!.color
          );
          break;
      }
      setColor(newColor);
    }
  }, [lists, listName, filteredTodos]);

  return (
    <div className="main-header-title-container">
      <h1 role={'heading'} className="headline-4" style={{ color }}>
        {listName.name}
      </h1>
      <h1
        data-testid="mainHeaderTitle-header2"
        className="headline-4"
        style={{ color }}
      >
        {filteredTodos.length}
      </h1>
    </div>
  );
};

export default MainHeaderTitle;
