import { Tag } from './tags';

export type Filter = {
  search: string;
  list: string;
  tags: Tag[];
  isShowing: boolean;
};
