import { useContext } from 'react';
import { ListsContext } from '../../../store/ListsContext';
import { TodosContext } from '../../../store/TodosContext';
import '../styles/lists.css';
import { ListRow } from './ListRow';

type ListsProps = {};

export const Lists = ({}: ListsProps) => {
  const { todos } = useContext(TodosContext);
  const { lists } = useContext(ListsContext);

  return (
    <>
      {lists
        .filter(
          (list) => list.name !== 'All' && list.name !== 'Flagged' && list
        )
        .sort((a, b) => a.createdAt - b.createdAt)
        .map((list) => (
          <ListRow
            key={list._id}
            list={list}
            quantity={
              todos.filter((todo) => todo.list.name === list.name).length
            }
          />
        ))}
    </>
  );
};
