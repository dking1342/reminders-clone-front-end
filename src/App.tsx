import { QueryClient, QueryClientProvider } from 'react-query';
import { FilterProvider } from './store/FiltersContext';
import { ListsProvider } from './store/ListsContext';
import { MenusProvider } from './store/MenusContext';
import { MobileProvider } from './store/MobileContext';
import { TagsProvider } from './store/TagsContext';
import { TodosProvider } from './store/TodosContext';
import Home from './views/components/Home';

const queryClient = new QueryClient();

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <MobileProvider>
        <MenusProvider>
          <TodosProvider>
            <ListsProvider>
              <TagsProvider>
                <FilterProvider>
                  <Home />
                </FilterProvider>
              </TagsProvider>
            </ListsProvider>
          </TodosProvider>
        </MenusProvider>
      </MobileProvider>
    </QueryClientProvider>
  );
};

export default App;
