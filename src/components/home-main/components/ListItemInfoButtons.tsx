import { useContext } from 'react';
import { useMutation } from 'react-query';
import {
  todoApiDeleteUrl,
  todoApiPutUrl,
} from '../../../api/todos/constants/urls';
import { fetchTodos } from '../../../api/todos/fetchRequest';
import { TodosContext } from '../../../store/TodosContext';
import { Todo } from '../../../types/todos';
import { EditButton } from '../../UI/components/EditButton';
import '../styles/listItemInfoButtons.css';

type ListItemInfoButtonsProps = {
  id: string | number | undefined;
};

export const ListItemInfoButtons = ({ id }: ListItemInfoButtonsProps) => {
  const { todos, handleUpdateTodoFlag, handleDeleteTodo } =
    useContext(TodosContext);
  const todo = todos.find((t) => t._id === id) as Todo;

  const todoFlagUpdateMutation = useMutation(
    (todoId: string | number) => {
      const url = todoApiPutUrl(todoId);
      return fetchTodos(url, 'PUT', { ...todo, isFlagged: !todo.isFlagged });
    },
    {
      onSuccess: (data, variables) => {
        if (data.status === 201) {
          handleUpdateTodoFlag(variables);
        }
      },
    }
  );

  const todoDeleteMutation = useMutation(
    (todoId: string | number) => {
      if (confirm('Are you sure you want to delete this todo?')) {
        const url = todoApiDeleteUrl(todoId);
        return fetchTodos(url, 'DELETE');
      } else {
        throw new Error('User chose not to delete the todo');
      }
    },
    {
      onSuccess: (data, variables) => {
        if (data.status === 204) {
          handleDeleteTodo(variables);
        }
      },
    }
  );

  return (
    <div className="todos-item-info-btn-try">
      <EditButton
        text="Flag"
        color="yellow"
        id={id}
        fn={todoFlagUpdateMutation.mutate}
      />
      <EditButton
        text="Delete"
        color="red"
        id={id}
        fn={todoDeleteMutation.mutate}
      />
    </div>
  );
};
