import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, screen } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { listData } from '../../../data/lists';
import { List } from '../../../types/lists';
import { ListRow } from '../components/ListRow';

type ListRowProps = {
  list: List;
  quantity: number;
};
let props: ListRowProps = {
  list: {} as List,
  quantity: 0,
};
const setup = () => {
  return render(
    <QueryClientProvider client={new QueryClient()}>
      <ListRow list={props.list} quantity={props.quantity} />
    </QueryClientProvider>
  );
};

describe('ListRow Component Tests', () => {
  test('Component container has class sidebar-list-row-container-open when isEditOpen is true', () => {
    setup();
    const className = 'sidebar-list-row-container-open';
    const btn = screen.getByTestId('listRowEditButton');
    expect(btn).toBeInTheDocument();
    fireEvent.click(btn);
    const container = screen.getByTestId('listRow');
    expect(container).toBeInTheDocument();
    const containerClassName = screen
      .getByTestId('listRow')
      .getAttribute('class');
    expect(containerClassName).toBe(className);
  });
  test('Component container has class sidebar-list-row-container when isEditOpen is false', () => {
    setup();
    const className = 'sidebar-list-row-container';
    const container = screen.getByTestId('listRow');
    expect(container).toBeInTheDocument();
    const containerClassName = screen
      .getByTestId('listRow')
      .getAttribute('class');
    expect(containerClassName).toBe(className);
  });
  test('Row quantity is showing 0', () => {
    setup();
    const quantity = '0';
    const quantityElement = screen.getByTestId('listRowBodyQuantity');
    expect(quantityElement).toBeInTheDocument();
    expect(quantityElement.textContent).toBe(quantity);
  });
  test('Row quantity is showing 0', () => {
    props = { ...props, list: listData[2] };
    setup();
    const listName = props.list.name;
    const rowName = screen.getByTestId('listRowBodyName');
    expect(rowName).toBeInTheDocument();
    expect(rowName.textContent).toBe(listName);
  });
});
