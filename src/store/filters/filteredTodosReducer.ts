import { Todo } from '../../types/todos';
import { FILTERS_CONSTANTS } from './filterConstants';

export const filteredTodosReducer = (
  state: Todo[],
  action: { type: string; payload: any }
) => {
  const { type, payload } = action;

  switch (type) {
    case FILTERS_CONSTANTS.FILTER_TODOS:
      const todos: Todo[] = payload;
      state = todos;
      return state;

    default:
      return state;
  }
};
