type AddIconProps = {};

export const AddIcon = ({}: AddIconProps) => {
  return (
    <svg
      viewBox="0 0 50 50"
      xmlns="http://www.w3.org/2000/svg"
      width={25}
      height={25}
    >
      <g stroke="white" strokeWidth={5} strokeLinecap="round">
        <line x1="25" y1="5" x2="25" y2="45" />
        <line x1="5" y1="25" x2="45" y2="25" />
      </g>
    </svg>
  );
};
