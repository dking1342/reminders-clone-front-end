import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { MenuAction } from '../../../types/menus';
import { Tag } from '../../../types/tags';
import { TagItem } from '../components/TagItem';

type Props = {
  tag: Tag;
  tags: Tag[];
  action?: MenuAction;
};
let props: Props = {
  tag: { _id: 111, name: '#jobs', isSelected: true } as Tag,
  tags: [],
};
const setup = () => {
  return render(<TagItem tag={props.tag} tags={props.tags} />);
};

describe('TagItem Component Tests', () => {
  test('The class tag-button-selected is present when isSelected is true', () => {
    const className = 'tag-button-selected';
    const isSelected = true;
    props = { ...props, tag: { ...props.tag, isSelected } };
    setup();
    const btn = screen.getByTestId('tagItem-btn');
    const btnClass = screen
      .getByTestId('tagItem-btn')
      .getAttribute('class')
      ?.split(' ')
      .includes(className);

    expect(btn).toBeInTheDocument();
    expect(btnClass).toBe(true);
  });
  test('The class tag-button-unselected is present when isSelected is false', () => {
    const className = 'tag-button-unselected';
    const isSelected = false;
    props = { ...props, tag: { ...props.tag, isSelected } };
    setup();
    const btn = screen.getByTestId('tagItem-btn');
    const btnClass = screen
      .getByTestId('tagItem-btn')
      .getAttribute('class')
      ?.split(' ')
      .includes(className);

    expect(btn).toBeInTheDocument();
    expect(btnClass).toBe(true);
  });
  test('Button has text of #jobs', () => {
    const text = '#jobs';
    setup();
    const btn = screen.getByTestId('tagItem-btn');
    expect(btn).toBeInTheDocument();
    expect(btn.textContent).toBe(text);
  });
});
